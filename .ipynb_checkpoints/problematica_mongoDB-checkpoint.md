# Problematica en MongoDB

En este documento se explican los inconvenientes de aplicar la resolucion del problema en MongoDB

## El Problema

Tengo una libro de asientos de 15 Millones de filas.

| fecha | cuenta | valor |

Para cada cuenta necesito calcular un resultado.Ese calculo requiere que recorra **"en orden cronologico"** las transacciones de las cuentas **"de principio a fin"**.

Si te interesa, amplia esta seccion que describo "La metodología de cálculo del score", por si quizás encontrás alguna solución no secuencial.

<details>
<sumary>La metodología de cálculo del score</sumary>

La metodologia para calcular el score, y asi reconocer la cuenta "exitosas", se basa en aquellas que con sus operaciones en el rango de tiempo analizado, hayan tenido ganancias (o perdidas) en sus transacciones.

* Las primeras ventas las descarto
* Las compras suman un "saldo" y mantienen un **"valor_promedio_de_compra"**
* Las ventas se limitan a descontar a lo sumo el **"saldo"** (es decir, no puede quedar saldo negativo)
* Las ventas que se permitan respetando el saldo positivo, calcularan una "diferencia". ( cant_vendida * ( valor_promedio_de_compra - valor_venta) ) 
  Esta differencia es positiva si gano o negativa si perdio.

Sumando las diferencias de las ventas da el valor al score.

(O relativizarlo con el valor total de las ventas)

</details>


## Intenos de Soluciones

### Intento Map Reduce

Al principio, quise llevarlo a cabo con un MapReduce.
Pero entendi (corregime si no es asi), que la resolucion secuencial no encaja en las "reglas" del MapReduce.

El problema esta en las siguientes caracteristicas que requiere la funcion Reduce, no se cumplen:
* Conmutativa
* Asociativa

(Al requerir orden, no se cumple)

<details>
<summary>Ver consulta</summary>

```javascript
  db.getCollection('ctacte').mapReduce(
  function(){ emit(this.address, {hash: this.hash, ts: this.ts, ethers: this.ethers, price: this.price}); },
  function(k,vs){ 
      
      // Aca ordeno vs e itero para realizar los calculos.
      // Esto  no funciono, porque este metodo se puede llamar mas de una vez y con distinto orden
      //Ej, si tuviera la cuenta A con transacciones [1,2,3,4,5,6]
      // podria llamarse 2 veces 
      //    (A, [1,2,3]
      //    (A, [reduce(A,[1,2,3]), 4,5,6]
      // Con lo cual, no me asegura que recibo TODAS las transacciones
      }
);
```

</details>


### Intento Aggregate Pipeline

Desechado el MapReduce, fui por la funcion "Aggregate Pipeline" que segun lei, se incorporo posteriormente y resulta mas eficiente que MR (en los casos que puede resolverse con ambas tecnicas) 

Aqui, utilizo un $group por ID de cuenta, y $push para armar un Array con las transacciones de la cuenta.

Esto me fallo, porque aun utilizando el parametro "allowDiskUse", la funcion $push (junto con $addToSet y alguna mas) tienen un limite de 100MB que se ve superado y aborta.

Cuando evite usar el $push, me daba otro error porque Mongo o permite objetos de mas de 16MB, y yo me encontre con que si bien la mayoria de las cuentas tiene entre 0 y 100 tx, habia algunas pocas que tenian mas de 200.000 tx. y por eso fallaba.

<details>
<summary>Ver consulta</summary>

```javascript
db.ctacte.aggregate([
{$group: {_id: "$address", count: {$sum: 1}, txs: {$push: {ts: "$ts", ethers: "$ethers", price: "$price"}}}},
],
{allowDiskUse: true}
).map(function (x) {
   
    //Aca realizaria el calculo con sus  "txs"
    //Me fallo el $push (limite 100MB)

})
```
</details>


### ForEach lineal

La otra opción es realizarlo linealmente ordenado por ID_CUENTA+ TIEMPO con un ForEach liso y llano.

*Asi lo hice con java mediante una herramienta de ETL que conozco mucho "Pentaho Data Integration" y demoro 10 minutos en procesar los 15 millones de registros. Recupeardos desde Posgres en orden.*


<details>
<summary>Ver consulta</summary>

```javascript
last_address = null;

db.ctacte.find().limit(1000).sort({address:1, ts:1}).forEach(
   function(x){

      if(last_address != x.address) {
         
          db.addresss.save({
              address: x.address,
              //valores calculados
              })
      }

  }
);
```
</details>

