echo "Descargando aplicaion...."
curl https://gitlab.com/dgraselli/ether-nauta/-/archive/master/ether-nauta-master.zip?path=ethernauta_app --output ethernauta.zip
unzip ethernauta.zip
mv ether-nauta-master-ethernauta_app/ethernauta_app/ ethernauta
rm -r ether-nauta-master-ethernauta_app
cd ethernauta

echo ""
echo ""
echo "-----------------------------------------------------------------------------------------"
echo "Ingresar a https://drive.google.com/drive/folders/12fFPWEH89YQNS7sLW-e475PZtT-eQ_zh"
echo "Descargar ambas carpetas"
echo "deben quedar los directorios 'mongo_data' y 'last_txs'"
echo ""
echo "presione ENTER para continuar"
read


echo ""
echo ""
echo "-----------------------------------------------------------------------------------------"
echo "Desplegando el stack de aplicaiones..."
docker-compose up -d

echo ""
echo ""
echo "-----------------------------------------------------------------------------------------"
echo "Cargando datos...."
bash load_mongo.sh
bash load_last_txs.sh

echo ""
echo ""
echo "-----------------------------------------------------------------------------------------"
echo "Listo !!"
echo "Acceda a http://localhost:8888"
echo ""
echo "Revise las secciones Informacion y ayuda"


