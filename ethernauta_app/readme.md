# Ethernauta App

Ethernauta es una aplicaion para recomendar el posicionamiento de inversión en Ethers, la criptomoneda de [Ethereum](https://ethereum.org/)

Basicamente, utiliza las transacciones y variaciones del precio de Ethers en un periodo reciente para identificar cuentas *"exitosas"*.
Luego, segun las decisiones tomadas por estas cuentas, realiza las recomendaciones de compra, venta o espera.

## Limitaciones

El lapso de tiempo en el que se analizaron las transacciones es fijo a el mes de Marzo de 2020.
La actualizacion de los datos, requiere una cuenta de google-api, ya que utiliza el servicio Big-Query para obtenerlos.

## Video informativo

En el siguiente [Link](https://www.loom.com/share/45403ebb37b34b248835ae58fd10acc8) hay un video que explica el funcionamiento de esta aplicaión.

## Vista previa

![Vista previa de la aplicacion][vista_previa]

[vista_previa]: ui/src/img/screen.gif "Vista previa de la aplicacion"

## Requisitos previos

Para correr la aplicacion se necesita tener instalado [Docker](http://www.docker.com) & [docker-compose](https://docs.docker.com/compose/) 

## Instalacion

Sigua las siguiente lista de pasos para poner a funcionar la aplicación

### 1-Descarga de la aplicacion

Descargar desde este [link](https://gitlab.com/dgraselli/ether-nauta/-/archive/master/ether-nauta-master.zip?path=ethernauta_app)

Descomprimir en un cualquier directorio vacio

### 2-Descarga los datos

Ingresar a https://drive.google.com/drive/folders/12fFPWEH89YQNS7sLW-e475PZtT-eQ_zh?usp=sharing

Descargar ambas carpetas, dentro del directorio de la aplicacion

Debe quedar asi:

```
  ./ethernauta
      ./mongo_data
      ./last_txs
```

### 2-Ejectuar

Ejecutar el siguiente comando para levantar el stack de aplicaciones

```bash
docker-compose up -d
```

### 3-Cargar la base de datos

En este paso, se cargan los datos a MongoDB.
Este paso demora varios minutos.

```bash
bash load_mongo.sh
bash load_last_txs.sh
```

### 4-Opcionalmente descargar datos de transacciones recientes

Al momento de probar la aplicacion, se posible que se encuentre desactualizados los datos de las 
ultimas transacciones.

La actualizacion de estas transacciones se realiza con un el script 'get_last_txs.sh', el cual genera un CSV para cargar en MongoDB.

Sin embargo, es script requiere la API-KEY de una cuenta en Google Cloud Platform (GCP) con facturacion habilitada, a pesar de que la consulta esta dentro de la capa gratuita.
Gooble Big Query tiene un limite de 1 TB mensual en su capa gratuita, y la consulta de las transacciones de los ultimos 7 dias consume 1,2 GB.

Para evitar requerir la cuenta, pueden solicitarme actualizar el CSV que se encuentra en GDrive en este [link](https://drive.google.com/drive/folders/12fFPWEH89YQNS7sLW-e475PZtT-eQ_zh?usp=sharing) y
luego cargarlo con el script 'load_last_txs.sh', que toma el CSV y actualiza la base MongoDB.

### Listo !!

Luego, se puede acceder directamente a la apliacion

[Ethernauta APP](http://localhost:8888)
http://localhost:8888

Tambien se deja libre acceso al stack completo de la aplicación que está compuesto por :

* Mongo 
* API Backend > http://localhost:8000
* Frontend > http://localhost:8888

y adicionalmente se incorporo Mongo Express para visualizar los datos :

* Mongo Express > http://localhost:8081

## Desarrollo del proyecto

En este apartado se describen las tecnicas y herramientas solo utilizdas para esta aplicacion.

Para informacion del proyecto referirse a [Informacíon general del proyecto](#informacíon-general-del-proyecto)

### Carga de Datos

Se utilizo el proyecto [ethereum-etl](https://github.com/blockchain-etl/ethereum-etl) para extraer las transacciones realizadas en la cadena de bloques (Blockchain) de Ethereum durante el periodo del 15-Feb al 15-Mar este año.

Mediante el soft PDI [Pentaho Data Integration]((https://www.hitachivantara.com/en-us/products/data-management-analytics/pentaho-platform/pentaho-data-integration.html)), de la suite Pentaho Comunity, se desarrollo el proceso ETL que carga el universo de analisis y complementa los datos con informacion de que permite identificar las cuentas mas 'exitosas'.

Este proceso, toma un CSV con el universo de transacciones y otro con el historico de precios.
Los une y ordena de manera ascendente por cuenta y fecha para poder procesar linealmente las operaciones realizadas y calcular las diferencias, tanto positivas como negativas obtenidas por las operaciones correspondientes.

Los datos se cargan a una base de datos MongoDB, que con los indices apropiados, es muy eficiente en devolver informacion relevante para el analisis.

Esquema del ETL:

![Diagrama][diagrama_etl]

[diagrama_etl]: ui/src/img/diagrama_etl.svg "diagrama ETL"

ETL PDI

![Diagrama][diagrama_etl_pdi1]
![Diagrama][diagrama_etl_pdi2]

[diagrama_etl_pdi1]: ui/src/img/pdi1.png "diagrama ETL PDI 1"
[diagrama_etl_pdi2]: ui/src/img/pdi2.png "diagrama ETL PDI 2"

### Visualización

La visualizacion de los datos se logro con un stack de aplicaciones React + un API en Node.js que accesde a MongoDB.

A continuacion se muestra el esquema del stack:


![App Stack][diagrama_stack]

[diagrama_stack]: ui/src/img/diagrama_stack.svg "stack"



Durante el desarrollo de esta aplicacion se utilizaron las siguientes herramientas:

* [Posgres SQL](https://www.postgresql.org/) como base de datos paralela para corroborar los resultados obtenidos en MongoDB.
* [Pentaho Data Integration](https://www.hitachivantara.com/en-us/products/data-management-analytics/pentaho-platform/pentaho-data-integration.html), como proceso ETL para la carga de datos a MongoDB
* [Node.js](https://nodejs.org/) como API backend
* [React](https://reactjs.org/) como frontend
* [Highcharts](highcharts.com) como libreria grafica
* [BigQuery](https://cloud.google.com/bigquery) para la recuperacion de trasacciones recientes
* [Visual Code](https://code.visualstudio.com/)para la edicion de codigo
* [Gitlab](https://gitlab.com/) para la gestion de versiones
* [Peek](https://github.com/phw/peek) para las pantallas animadas
* [Loom](https://www.loom.com/) para la grabacion de videos
* [Ubuntu Mint](https://www.linuxmint.com/) como Sistema operativo

## Complicaciones

Tuvimos varios inconvenientes para realizar en MongoDB el analisis y seleccion de cuentas.
Probablemente por falta de experiencia, pero aun asi, el manejo de memoria y limtiaciones intrisecas del software nos impidieron realizar esta tarea. Por lo cual decidimos utilizar Pentaho Data Integration para la carga y analisis de las cuentas.

En el siguiente link se explican los intentos de solucion con MongoDB

[Problematica con MongoDB](https://gitlab.com/dgraselli/ether-nauta/-/blob/master/problematica_mongoDB.md)

## Informacíon general del proyecto

En el siguiente link se describen con detalle los aspectos del pryecto.

[link al Readme de Gitlab](https://gitlab.com/dgraselli/ether-nauta/)