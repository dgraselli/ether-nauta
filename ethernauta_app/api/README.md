# Ethernauta Backend

API de acceso a la informacoin almacenada en MongoDB

## Colecciones utilizadas

* **transactions** *(Transacciones del periodo de analisis)*
* **addresses** *(Cuentas con su ganancia correspondienete en el periodo de analisis)*
* **last_transactions** *(Transacciones recientes)*

## Start

API start

> node start

Dev API start

> nodemon start

## Indices requeridos en MongoDB

> db.prices.createIndex({ts:1})
> db.addresses.createIndex({address:1});
> db.addresses.createIndex({diff:1});
> db.addresses.createIndex({diff_per:1});
> db.transactions.createIndex({address:1});
> db.transactions.createIndex({ts:1});
> db.last_transactions.createIndex({address:1});
> db.last_transactions.createIndex({ts:1});