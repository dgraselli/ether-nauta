const mongoose = require('mongoose');

const addressSchema = new mongoose.Schema({
    address: String,
    diff: Number,
    diff_per: Number
})


const Address = mongoose.model('address', addressSchema);

//Retorna las cuentas mas ....
//  t=top => Aquellas "n" con mayor ganancia .
//  t=bottom => Aquellas "n" con mayor ganancia .
//  t=itop => Aquellas "n" con mayor diferencia porcentual entre las "m" con mayor ganancia .
//  t=ibottom => Aquellas "n" con mayor diferencia porcentual negativa entre las "m" con menor ganancia .
Address.list = (t='atop', n=10,m=1000) => {
    let pipeline = [];
    switch (t) {
        case 'atop':
            pipeline.push({$sort: {diff: -1}});
            break;
        case 'abottom':
            pipeline.push({$sort: {diff: 1}});
            break;
        case 'rtop':
            pipeline.push({$sort: {diff: -1}});
            pipeline.push({$limit: parseInt(m)});
            pipeline.push({$sort: {diff_per: -1}});
            break;
        case 'rbottom':
            pipeline.push({$sort: {diff: 1}});
            pipeline.push({$limit: parseInt(m)});
            pipeline.push({$sort: {diff_per: 1}});
            break;
            
        default:
            throw "tipo desconocido. utilice [atop, abottom, rtop ,rbottom]";
    }

    pipeline.push({$project: {address:1, diff:1, diff_per:1, cant_op:1}});
    pipeline.push({$limit: parseInt(n)});

    return Address.aggregate(pipeline);
}




//Retorna las cuentas agrupadas por diferencias
Address.histogram_diff = (n) => {
    return Address.aggregate( [
      {$bucketAuto: {
          groupBy: "$diff",
          buckets: n,
          output: {
            "diff": { $sum: "$diff" },
            "diff_per": { $avg: "$diff_per" },
        }
      }}
      ]
      ).allowDiskUse(true)
}



module.exports = Address;