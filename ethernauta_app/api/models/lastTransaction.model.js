const mongoose = require('mongoose');

const transactionSchema = new mongoose.Schema({
    address: String,
    ts: Number,
    ethers: Number
})


const LastTransaction = mongoose.model('last_transaction', transactionSchema);



module.exports = LastTransaction;