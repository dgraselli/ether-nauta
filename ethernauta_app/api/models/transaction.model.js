const mongoose = require('mongoose');

const transactionSchema = new mongoose.Schema({
    address: String,
    ts: Number,
    ethers: Number
})


const Transaction = mongoose.model('transaction', transactionSchema);



module.exports = Transaction;