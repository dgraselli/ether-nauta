const router = require('express').Router();
const mongoose = require('mongoose');
let Addresses = require('../models/address.model');
let Transaction = require('../models/transaction.model');

Prices = mongoose.model('price', new mongoose.Schema({price: Number}));

router.route('/:a?-?:b?').get((req, res) => {
    const a = req.params.a || new Date('2020-02-15').getTime() / 1000;
    const b = req.params.a || new Date('2020-03-15').getTime() / 1000;
    Prices.aggregate([
       {$match: {ts: {$gt: a, $lt: b}}},
       {$group: {
         _id: {$subtract: [{$toDouble:"$ts"}, {$mod: ["$ts", 60*60*24]}] },
         price: {$avg: "$highPrice_ask"}
       }},
       {$project: {
        "_id": 0, 
        "ts": "$_id",
        "price": "$price",
        },
       },
       {$sort: {ts:1}},
       
      ])
      .then(ret => res.json(ret))
      .catch(err => res.status(400).json('Error: '+err));
  });

module.exports = router;