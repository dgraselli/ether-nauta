const router = require('express').Router();
const mongoose = require('mongoose');
let Address = require('../models/address.model');
let Transaction = require('../models/transaction.model');


router.route('/:s?').get((req, res) => {
  Address.list.apply(null, req.params.s.split('-'))
    .then(ret => res.json(ret))
    .catch(err => res.status(400).json('Error: ' + err));
})

router.route('/:address/transactions:n?').get((req, res) => {
  Transaction.find({ 'address': req.params.address })
  .select({ _id:0, hash:1, ts:1, ethers:1, price:1, balance_e:1, buy_avg:1, diff:1, diff_e:1, diff_per:1 })
  .limit(parseInt(req.params.n) || 100) //.sort({'diff': -1})
    .limit(parseInt(req.params.n) || 1000)
    .then(users => res.json(users))
    .catch(err => res.status(400).json('Error: ' + err));
})



//g-transactoins Agrupadas por fecha
router.route('/:address/gtransactions:n?').get((req, res) => {
  const n = parseInt(req.params.n || 1000);

  Transaction.aggregate([
    {$match: { address: req.params.address } },
    {$group: {
      _id: {$subtract: [{$toDouble:"$ts"}, {$mod: ["$ts", 60*60*24]}] },
      ethers: {$sum: "$ethers"},
      diff: {$sum: "$diff"},
      diff_per: {$sum: "$diff_per"},
    }},
    {$sort: {ts:1}},
    {$limit: n},
    {$project: {
      "_id": 0, 
      "ts": "$_id",
      "ethers": "$ethers",
      "diff": "$diff",
      "diff_per": "$diff_per",
      },
     },
  ])
    .then(ret => res.json(ret))
    .catch(err => res.status(400).json('Error: ' + err));
});


module.exports = router;