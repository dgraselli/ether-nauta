const router = require("express").Router();
const mongoose = require("mongoose");
const Addresses = require("../models/address.model");
const LastTransaction = require("../models/lastTransaction.model");
let Transaction = require("../models/transaction.model");

//Retorna informacion operaciones realizadas desde hace "m"
// por las "n" cuentas mas exitosas
router.route("/summary_top:n/:m?").get((req, res) => {
  const n = parseInt(req.params.n || 100);
  const m = parseInt(req.params.m || 60 * 24); //deafult ultimas 24 horas

  //resta a la fecha actual m minutos
  const since_minutes_before = new Date().getTime() / 1000 - m * 60;
  
  Addresses.find()
    .sort({ diff: -1 })
    .limit(1000)
    .sort({ diff_per: -1 })
    .limit(n)
    .then((ret) => {
      const addresses = ret.map((x) => x.address);
      LastTransaction.aggregate([
        {
          $match: {
            address: { $in: addresses },
            ts: { $gt: since_minutes_before },
          },
        },
        {
          $group: {
            _id: null,
            ts_min: { $min: "$ts" },
            ts_max: { $max: "$ts" },
            addresses: { $addToSet: "$address" },
            in_addresses: {
              $addToSet: { $cond: [{ $gt: ["$ethers", 0] }, "$address", null] },
            },
            out_addresses: {
              $addToSet: { $cond: [{ $lt: ["$ethers", 0] }, "$address", null] },
            },
            e_in: {
              $sum: {
                $cond: [{ $gt: ["$ethers", 0] }, "$ethers", 0],
              },
            },
            e_out: {
              $sum: {
                $cond: [{ $lt: ["$ethers", 0] }, "$ethers", 0],
              },
            },
            op_in: {
              $sum: {
                $cond: [{ $gt: ["$ethers", 0] }, 1, 0],
              },
            },
            op_out: {
              $sum: {
                $cond: [{ $lt: ["$ethers", 0] }, 1, 0],
              },
            },
          },
        },
        {
          $project: {
            cant_add: { $size: "$addresses" },
            cant_add_in: { $size: "$in_addresses" },
            cant_add_out: { $size: "$out_addresses" },
            op_in: "$op_in",
            op_out: "$op_out",
            e_in: "$e_in",
            e_out: "$e_out",
            ts_min: "$ts_min",
            ts_max: "$ts_max",
          },
        },
        {$sort: {ts: 1}}
      ])
        .then((ret) => res.json(ret[0]))
        .catch((err) => res.status(400).json("Error: " + err));
    })
    .catch((err) => res.status(400).json("Error: " + err));
});

//Retorna informacion operaciones realizadas desde hace "m"
// por las "n" cuentas mas exitosas
router.route("/history_top:n/:m?").get((req, res) => {
  const n = parseInt(req.params.n || 100);
  const m = parseInt(req.params.m || 60 * 24); //deafult ultimas 24 horas

  //resta a la fecha actual m minutos
  const since_minutes_before = new Date().getTime() / 1000 - m * 60;

  Addresses.find()
    .sort({ diff: -1 })
    .limit(1000)
    .sort({ diff_per: -1 })
    .limit(n)
    .then((ret) => {
      const addresses = ret.map((x) => x.address);
      LastTransaction.aggregate([
        { $match: { address: { $in: addresses }, ts: {$gt: since_minutes_before} } },
        {
          $group: {
            _id: { $subtract: ["$ts", { $mod: ["$ts", 60 * 60] }] },
            e_in: {
              $sum: {
                $cond: [{ $gt: ["$ethers", 0] }, "$ethers", 0],
              },
            },
            e_out: {
              $sum: {
                $cond: [{ $lt: ["$ethers", 0] }, "$ethers", 0],
              },
            },
            op_in: {
              $sum: {
                $cond: [{ $gt: ["$ethers", 0] }, 1, 0],
              },
            },
            op_out: {
              $sum: {
                $cond: [{ $lt: ["$ethers", 0] }, 1, 0],
              },
            },
          },
        },
        {$project: {
          _id: 0, 
          ts: "$_id",
          op_in: "$op_in",
          op_out: "$op_out",
          e_in: "$e_in",                
          e_out: "$e_out",                
        }},
        {$sort: {ts: 1}}
      ])
        .then((ret) => res.json(ret))
        .catch((err) => res.status(400).json("Error: " + err));
    })
    .catch((err) => res.status(400).json("Error: " + err));
});


router.route("/txs_top:n/:m?").get((req, res) => {
  const n = parseInt(req.params.n || 100);
  const m = parseInt(req.params.m || 60 * 24); //deafult ultimas 24 horas

  //resta a la fecha actual m minutos
  const since_minutes_before = new Date().getTime() / 1000 - m * 60;

  Addresses.find()
    .sort({ diff: -1 })
    .limit(1000)
    .sort({ diff_per: -1 })
    .limit(n)
    .then((ret) => {
      const addresses = ret.map((x) => x.address);
      LastTransaction.aggregate([
        {
          $match: {
            address: { $in: addresses },
            ts: { $gt: since_minutes_before },
          },
        },
        {$sort: {ts: 1}}
      ])
        .then((ret) => res.json(ret))
        .catch((err) => res.status(400).json("Error: " + err));
    })
    .catch((err) => res.status(400).json("Error: " + err));
});


router.route("/history_by_address_top:n/:m?").get((req, res) => {
  const n = parseInt(req.params.n || 100);
  const m = parseInt(req.params.m || 60 * 24); //deafult ultimas 24 horas

  //resta a la fecha actual m minutos
  const since_minutes_before = new Date().getTime() / 1000 - m * 60;

  Addresses.find()
    .sort({ diff: -1 })
    .limit(1000)
    .sort({ diff_per: -1 })
    .limit(n)
    .then((ret) => {
      const addresses = ret.map((x) => x.address);
      LastTransaction.aggregate([
        {$match: {ts: {$gt: since_minutes_before} } },
        {$addFields:{top: { $in:["$address", addresses] }}},
        {
          $group: {
            //_id: {ts:{$subtract: ["$ts", { $mod: ["$ts", 60 * 60] }] }},
            _id: {ts:{$subtract: ["$ts", { $mod: ["$ts", 60 * 60] }] }, top: "$top"},
            //_id: {address: "$top"},
            e: {
              $sum: "$ethers",
            },
            op: {
              $sum: 1,
            },
          },
        },
         {$project: {
          _id: 0, 
          ts: "$_id.ts", 
          top: "$_id.top", 
          op: "$op",
          e: "$e",
         }},
      ])
        .then((ret) => res.json(ret))
        .catch((err) => res.status(400).json("Error: " + err));
    })
    .catch((err) => res.status(400).json("Error: " + err));
});

module.exports = router;
