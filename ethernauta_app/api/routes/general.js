const router = require('express').Router();
const mongoose = require('mongoose');
let Address = require('../models/address.model');
let Transaction = require('../models/transaction.model');


router.route('/histogram_diff:n?').get((req, res) => {
  Address.histogram_diff(parseInt(req.params.n, 10) || 10)
    .then(ret => res.json(ret))
    .catch(err => res.status(400).json('Error: '+err));
})

router.route('/data_exists').get((req, res) => {
  Address.find().count()
    .then(ret => res.json(ret > 0))
    .catch(err => res.status(400).json('Error: '+err));
})
  

module.exports = router;