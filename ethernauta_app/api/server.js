require('dotenv').config();
const express = require('express');
const ExpressCache = require('express-cache-middleware')
const cacheManager = require('cache-manager')
const corrs = require('cors');
const mongoose = require('mongoose');

uri = process.env.DB_URI;
mongoose.connect(uri, {useNewUrlParser:true, useCreateIndex: true, useUnifiedTopology: true});
const mc = mongoose.connection;
mc.once('open',() => {console.log('Mongo OK');});

const app = express();
const port = process.env.PORT || 8080;


//Cache middleware
const cacheMiddleware = new ExpressCache(
    cacheManager.caching({
        store: 'memory', max: 10000, ttl: 3600
    })
)
// Layer the caching in front of the other routes
if(process.env.ENABLE_CACHE==1){
    console.log("Enable CACHE")
    cacheMiddleware.attach(app)
}else{console.log("Disable CACHE")}

app.use(corrs());
app.use(express.json());


const generalRouter = require('./routes/general')
const addresessRouter = require('./routes/address')
const transactionsRouter = require('./routes/transactions')
const pricesRouter = require('./routes/prices')

app.use('/', generalRouter)
app.use('/a', addresessRouter)
app.use('/t',  transactionsRouter)
app.use('/prices', pricesRouter)




app.listen(port, () => {
    console.log(`Server listen on port ${port}`);
})