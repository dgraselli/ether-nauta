require("dotenv").config();

if(!process.env.GOOGLE_APPLICATION_CREDENTIALS){
  throw "Debe setear la variable: GOOGLE_APPLICATION_CREDENTIALS";
}
const dias = 7
const args = process.argv.slice(2);
const csv_filename = args[0];

if (!csv_filename){
  throw "Debe especificar el nombre del archivo csv como parametro"
}

const createCsvWriter = require('csv-writer').createObjectCsvWriter;
const { BigQuery } = require("@google-cloud/bigquery");
const bigquery = new BigQuery();


async function update_last_transactions(days = 7) {
  // Queries the U.S. given names dataset for the state of Texas.

  const query = `
  with
 --limita a ciertas cuentas ?
 --SELECTE_ADD as (
 --   SELECT address  FROM \`ether-nauta.data.top100\`  
    --order by diff desc LIMIT 100
 --),
 TXS as (
  select txs.hash, block_timestamp, from_address, to_address, value/pow(10,18) as ethers, 
  from \`bigquery-public-data.crypto_ethereum.transactions\` txs
  where value > 0
  and txs.receipt_status = 1
  and txs.from_address is not null and txs.to_address is not null
  -- limita a las transacciones desde ayer
  and  date_trunc(date(block_timestamp), day) >= date_trunc(date_add(current_date(), interval -${days} day), day)
  --and (txs.from_address in (select address from SELECTE_ADD)  or  txs.to_address in (select address from SELECTE_ADD)  )
  ),

TXS_OUT as (
  -- solo transacciones de salida. 
  select txs.hash, block_timestamp ts, from_address address, -ethers ethers
  from TXS
),

TXS_IN as (
  -- solo transacciones de entrada. 
  select txs.hash, block_timestamp ts, to_address address, ethers
  from TXS
),

TXS_ALL as (
    -- unifica entradas y salidas
    select * from TXS_OUT
    union all
    select * from TXS_IN
    )

select address, ts, txs_all.hash, ethers
from txs_all
order by address,ts desc
`;

  // For all options, see https://cloud.google.com/bigquery/docs/reference/rest/v2/jobs/query
  const options = {
    query: query,
    // Location must match that of the dataset(s) referenced in the query.
    location: "US",
  };

  // Run the query as a job
  const [job] = await bigquery.createQueryJob(options);
  console.log(`Job ${job.id} started.`);

  // Wait for the query to finish
  const [rows] = await job.getQueryResults();

  // Print the results
  console.log("Rows:");
  console.log(rows.length + " registros");

  regs = rows.map((row) => ({
    ts: Math.round(new Date(row.ts.value).getTime() / 1000),
    hash: row.hash,
    address: row.address,
    ethers: row.ethers,
  }));

  const csvWriter = createCsvWriter({
    path: csv_filename,
    header: [
      {id: 'hash', title: 'hash'},
      {id: 'address', title: 'address'},
      {id: 'ethers', title: 'ethers'},
      {id: 'ts', title: 'ts'},
    ]
  });


  csvWriter
  .writeRecords(regs)
  .then(()=> console.log('The CSV file was written successfully'));

  //console.log(regs);
  //await mc.dropCollection('last_transactions');
  //await LastTransaction.insertMany(regs);

}

update_last_transactions(dias)
.then(e=>console.log("OK"))
.catch(e=>console.log(e))


