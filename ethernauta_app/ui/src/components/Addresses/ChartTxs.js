import React from "react";
import Highcharts from "highcharts/highstock";
import HighchartsReact from "highcharts-react-official";
import axios from 'axios';

class ChartTxs extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            data: [],
            data_diff: [],
            data_prices: [],
        };
    }

    async componentDidMount() {

        const url = process.env.REACT_APP_API_URL;
        const n = 1000// (this.props.n || 100);

        const res1 = await axios.get(url + '/a/' + this.props.address + "/gtransactions" + n);
        const data = res1.data.map(x => { return [x.ts*1000, x.ethers] });
        const data_diff = res1.data.map(x => { return [x.ts*1000, x.diff] });

        const min = data.reduce((a,b)=>Math.min(a,b.ts));
        const max = data.reduce((a,b)=>Math.max(a,b.ts));


        const res2 = await axios.get(url + `/prices`);
        const data_prices = res2.data.map(x => { return [x.ts*1000, x.price] });

        this.setState({data, data_diff, data_prices})

    }

    render() {

        const options = {
            //chart: { type: 'column' },
            title: { text: ''},
            subtitle: { text: this.props.subtitle || 'del periodo' },
            xAxis: {
                type: 'datetime',
            },
            yAxis: [
                {
                    title: {
                        text: 'Ethers'
                    },
                },
                {
                    title: {
                        text: 'Ganancia en U$S'
                    },
                },
                {
                    title: {
                        text: 'Precio en U$S'
                    },
                    opposite: true,
                },
            ],
            plotOptions: {
                series: {
                    marker: {
                        enabled: true
                    }
                }
            },
            //colors: ['#6CF', '#39F', '#06C', '#036', '#000'],
            series: [
                {
                    name: 'Transacciones top',
                    type: 'column',
                    data: this.state.data,
                },
                {
                    name: 'Ganancia',
                    type: 'column',
                    color: 'green',
                    yAxis: 1,
                    data: this.state.data_diff,
                },
                {
                    name: 'Precio de Ether',
                    type: 'spline',
                    yAxis: 2,
                    data: this.state.data_prices,
                },
            ],
            chart: {
                //type: 'column',
                height: this.props.heght || 250,
                
              },
        };

        return (
            <HighchartsReact
                highcharts={Highcharts}
                constructorType={"chart"}
                options={options}
            />
        );
    }
}

export default ChartTxs;