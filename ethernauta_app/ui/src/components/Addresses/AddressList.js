import React, { useState, useEffect } from "react";
import axios from "axios";
import { Table, TableContainer, TableHead, TableRow, TableCell, TableBody, Paper } from "@material-ui/core";
import { FormatPercent, FormatMagicNumber, FormatCurrency } from "../common";


export default function AddressList(props) {
    const [addresses, setAddresses] = useState([]);


    useEffect(() => {
        let link =
            process.env.REACT_APP_API_URL + '/a/' +
            (props.tipo || "top") +
            "-" +
            (props.n || 10);
        axios
            .get(link)
            .then((res) => {
                setAddresses(res.data);
            })
            .catch((err) => {
                console.log(err);
            });
    },[props]);

    
    return (
        <div className="container" m={10}>
            <h3 style={{ color: props.color }}>{props.title}</h3>

        	<TableContainer component={Paper}  >
              <Table size="small">
                <TableHead>
                  <TableRow>
                    <TableCell></TableCell>
                    <TableCell>Address</TableCell>
                    <TableCell align="right">#op</TableCell>
                    <TableCell align="right">$</TableCell>
                    <TableCell align="right">%</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                {addresses.map((address, idx) => {
                    const rclr = address.diff > 0? 'green': 'red';
                    return (
                    <TableRow key={idx} style={{cursor: "pointer"}} onClick={() => props.onAddressSelected(address)}>
                      <TableCell>{idx+1}</TableCell>
                      <TableCell>
                            {address.address.substr(0, 4)}..
                                {address.address.substr(-4)}
                            
                      </TableCell>
                      <TableCell align="right" >
                      {FormatMagicNumber(address.cant_op) }
                      </TableCell>
                      <TableCell align="right" style={{color: rclr}}>
                      {/* {FormatMagicCurrency(address.diff) } */}
                      {FormatCurrency(address.diff) }
                      </TableCell>
                      <TableCell align="right" style={{color: rclr}}>
                      {/* {FormatMagicCurrency(address.diff_per) } */}
                      {FormatPercent(address.diff_per/100) }
                      </TableCell>
                    </TableRow>
                    )}
                  )}
                </TableBody>
              </Table>
            </TableContainer>
        </div>

    );
}
