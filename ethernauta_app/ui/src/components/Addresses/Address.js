import React, { useState, useEffect } from "react";
import Axios from "axios";
import {
    FormatCurrency,
    FormatPercent,
    FormatNumber,
    FormatDate,
} from "../common";
import {
    Box,
    Grid,
    TableCell,
    TableBody,
    TableRow,
    TableHead,
    Table,
    TableContainer,
    Switch,
} from "@material-ui/core";
import CustomCard from "../CustomCard";
import ChartTxs from "./ChartTxs";


export default function Address(props) {
    const [addresses, setAddresses] = useState([]);
    const [filters, setFilters] = useState(true);


    
    useEffect(() => {
        let link = `${process.env.REACT_APP_API_URL}/a/${props.address.address}/transactions`;
        
        Axios.get(link)
            .then((res) => {
                setAddresses(res.data);
            })
            .catch((err) => {
                console.log(err);
            });
    }, [props]);

    const getAddress = () => {
        return addresses.filter((a) => !filters || a.diff !== 0).sort((a,b)=>(a.ts - b.ts));
    };

    if (!props.address) return "";

    return (
        <React.Fragment>
            <Grid container
            >
                <Grid item xs={9}>
                    <ChartTxs n={props.n} address={props.address.address}></ChartTxs>
                </Grid>
                <Grid item xs={3}>
                    <Grid container
                                alignContent="stretch"
                                justify="center"
                    >
                        <CustomCard title="Cantidad de Operaciones">
                            {props.address.cant_op}
                        </CustomCard>
                        <CustomCard title="Ganancia Neta">
                            {FormatCurrency(props.address.diff)}
                        </CustomCard>
                        <CustomCard title="Ganancia Porcentual">
                            {FormatPercent(props.address.diff_per/100)}
                        </CustomCard>
                    </Grid>
                </Grid>
                <Grid item xs={12}>
            <h5 style={{borderBottom: '1px solid gray'}}>Operaciones</h5>
            <TableContainer>
                <Box style={{float: "left"}}>
                <label>Visualizar solo las operaciones con diferencia</label>
                <Switch 
                    checked={filters}
                    onChange={(e) => setFilters(e.target.checked)}
                />
                </Box>
                <Table size="small">
                    <TableHead>
                        <TableRow>
                            <TableCell>Hash</TableCell>
                            <TableCell>Fecha</TableCell>
                            <TableCell>Operacion</TableCell>
                            <TableCell>Ethers</TableCell>
                            <TableCell>Precio</TableCell>
                            <TableCell>Saldo positivo</TableCell>
                            <TableCell>Precio Prom. de Compra</TableCell>
                            <TableCell>Ganancia en $</TableCell>
                            <TableCell>Ganancia en %</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {getAddress().map((ad, idx) => {
                            let color = ad.diff>0 ? 'green' : 'red';
                            return (
                                <TableRow key={idx}>
                                    <TableCell>
                                        <a target="_blank" rel="noopener noreferrer" href={`https://etherscan.io/tx/${ad.hash}`}>
                                        {ad.hash.substr(0, 4)}..
                                        {ad.hash.substr(-4)}
                                        </a>
                                    </TableCell>
                                    <TableCell>{FormatDate(ad.ts)}</TableCell>
                                    <TableCell>
                                        {ad.ethers > 0 ? (
                                            <Box color="gray">COMPRA</Box>
                                        ) : (
                                            <Box color="blue">VENTA</Box>
                                        )}
                                    </TableCell>
                                    <TableCell align="right">
                                        {FormatNumber(ad.ethers)}
                                    </TableCell>
                                    <TableCell align="right">
                                        {FormatCurrency(ad.price)}
                                    </TableCell>
                                    <TableCell align="right">
                                        {FormatNumber(ad.balance_e)}
                                    </TableCell>
                                    <TableCell align="right">
                                        {FormatCurrency(ad.buy_avg)}
                                    </TableCell>
                                    <TableCell align="right" style={{color: color}}>
                                        {ad.diff !== 0 && FormatCurrency(ad.diff)}
                                    </TableCell>
                                    <TableCell align="right" style={{color: color}}>
                                        {ad.diff !== 0 &&
                                            FormatPercent(ad.diff_per/100)}
                                    </TableCell>
                                </TableRow>
                            );
                        })}
                    </TableBody>
                </Table>
            </TableContainer>
            </Grid>
            </Grid>

        </React.Fragment>
    );
}
