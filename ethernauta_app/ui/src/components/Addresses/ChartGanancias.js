import React, { useEffect, useState } from "react";
import HighchartsReact from "highcharts-react-official";
import axios from "axios";

export default function Chartganancias(props) {
    const [data, setData] = useState([]);

    useEffect(() => {
        (async function mm() {
            let url = `${process.env.REACT_APP_API_URL}/a/${props.tipo}-${props.n}`;
            console.log(url);

            let res = await axios.get(
                url
            );
            let data = res.data.map((x) => {
                return props.tipo[0]==='a'? x.diff : x.diff_per;
            }).sort((a,b)=> b-a)

            console.table(data);
            setData(data);
        })();
    }, [props]);

    const options = {
        title: {
            text: null,
        },
        series: [
            {
                name: "Ganancias",
                data: data,
                color: props.color,
                tooltip: {
                    valueDecimals: 0,
                },
            },
        ],
        chart: {
            type: "bar",
            horizontal: true,
            height: 200,
        },
        yAxis: {
            //type: "logarithmic",
            enabled: false,
        },
        legend: { enabled: false },
    };

    return <HighchartsReact options={options} />;
}
