import React from "react";
import './CustomCard.css';

export default function CustomCard(props) {
    
    
    
    return (
        <div className="card">
            <div className="body">
                {props.children}
            </div>
            <div className="title">
                {props.title}
            </div>
        </div>
    );
}
