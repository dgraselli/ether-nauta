export const modo_de_uso = `

`

export const info = `
# Proyecto

Este trabajo esta vinculado a la materia **"Captura y Analisis de datos"** de la especialidad **"Especialista en Inteligencia de Datos orientada a Big Data"** 
dictada en la Universidad Nacional de La Plata (UNLP) 


## Objetivo

Brindar un **"Sistema de Recomendacion"** que indique que hacer con una inversion en la cripto-moneda Ethereum.

* Comprar 
* Vender
* Mantener

## Alcance

* **Capturar** en un *periodo de analisis*, información pública de las transacciones de la cadena de bloques de Ethereum + informacion adicional.

* **Capturar** los precios de mercado del valor de la criptomoneda para poder vincularos a las transacciones realizadas segun el tiempo en que se realizaron.

* **Procesar** esta informacion para detectar las **'cuentas mas Exitosas'** 

* **Capturar** las *oparaciones mas recientes* para analizar que comportamiento estan teniendo las "cuentas exitosas"

Un indicador, basado en esta informacion establecerá la recomendación.

## Detalles de la implementación (TL/DR)

En el siguiente link, se encontrará la informacion completa referida al proyecto

[Link al Readme de Github](https://gitlab.com/dgraselli/ether-nauta/-/blob/master/README.md)

## Alumnos

* Emanuel Borda
* Diego Graselli
`