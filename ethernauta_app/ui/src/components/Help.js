import React from "react";
import Grid from "@material-ui/core/Grid";
import screen from "../img/screen.gif";

export default function Help() {
    return (
        <Grid style={{ textAlign: "center" }}>
            <h1>Ayuda</h1>
            <p>
               En <b>Analisis de las cuentas</b>, se visualiza la información del periodo de analisis (Marzo 2020),
                donde se identifican las 'cuentas exitosas'.<br />
               En <b>Analisis de Op Recientes</b>, se visualiza un resumen de las operaciones recientemente realizadas (ultimo dia) por estas cuentas.
               <br />
               Finalimente, en <b>Recomendaciones</b>, se muestra un panel indicando que recomendacion seguir.
            </p>

            <div>
                <img alt="animacion" className="screen" src={screen}></img>
            </div>
        </Grid>
    );
}
