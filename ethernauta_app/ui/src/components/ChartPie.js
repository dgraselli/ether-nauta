import React from 'react'
import HighchartsReact from 'highcharts-react-official'

export default function ChartPie(props) {
    
    const custom_options = {
        chart: {
            type: 'pie',
            height: props.heght || 150,
        },
        title: {
            text: props.title,
        },
        series:props.series
    }
    return (
        <HighchartsReact 
            options={{...props.options, ...custom_options}}
        ></HighchartsReact>
    )
}
