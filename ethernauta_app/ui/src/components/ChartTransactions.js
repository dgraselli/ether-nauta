import React from "react";
import HighchartsReact from "highcharts-react-official";

export default function ChartTransactions(props) {
    const options = {
        //chart: { type: 'column' },
        title: { text: "" },
        xAxis: {
            type: "datetime",
        },
        yAxis: [
            {
                title: {
                    text: "Ethers",
                },
            },
        ],
        series: [
            {
                name: "Compras",
                color: "green",
                data: props.chart_transactions,
            },
            // {
            //     name: "Ventas",
            //     color: 'red',
            //     data: data_out,
            // },
        ],
        chart: {
            type: "column",
            height: props.heght || 250,
        },
    };

    return (
        <div>
            <HighchartsReact options={options} />
        </div>
    );
}
