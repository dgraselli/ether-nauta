import React from "react";
import { Container } from "@material-ui/core";
import { Markdown } from "./common";
const md = require("./docs_md");

export default function Info() {
    /*     
    const [info, setInfo] = useState('');

    useEffect(() => {
        Axios.get('https://gitlab.com/dgraselli/ether-nauta/-/raw/master/README.md')
        .then(ret=>setInfo(ret.data))
        .catch(err=>console.log(err));
    }, []) 
    */

    return (
        <Container style={{ margin: 20, width: "80%" }}>
            <h1>ETHERNAUTA</h1>
            <br /><br />
            <Markdown>{md.info}</Markdown>
        </Container>
    );
}
