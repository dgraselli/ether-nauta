import React from 'react'
import { Table, TableContainer, Box, TableHead, TableRow, TableCell, TableBody } from '@material-ui/core';
import { FormatNumber, FormatDate } from '../common';

export default function TransactionsList(props) {

    return (
        <TableContainer>
        <Table size="small">
            <TableHead>
                <TableRow>
                    <TableCell>Hash</TableCell>
                    <TableCell>Address</TableCell>
                    <TableCell>Fecha</TableCell>
                    <TableCell>Operacion</TableCell>
                    <TableCell>Ethers</TableCell>
                </TableRow>
            </TableHead>
            <TableBody>
                {props.transactions && props.transactions.map((ad, idx) => {
                    return (
                        <TableRow key={idx}>
                            <TableCell>
                                <a target="_blank" rel="noopener noreferrer" href={`https://etherscan.io/tx/${ad.hash}`}>
                                {ad.hash.substr(0, 4)}..
                                {ad.hash.substr(-4)}
                                </a>
                            </TableCell>
                            <TableCell>
                            <a target="_blank" rel="noopener noreferrer" href={`https://etherscan.io/address/${ad.address}`}>
                                {ad.address.substr(0, 4)}..
                                {ad.address.substr(-4)}
                                </a>
                            </TableCell>
                            <TableCell>{FormatDate(ad.ts)}</TableCell>
                            <TableCell>
                                {ad.ethers > 0 ? (
                                    <Box color="green">COMPRA</Box>
                                ) : (
                                    <Box color="red">VENTA</Box>
                                )}
                            </TableCell>
                            <TableCell align="right">
                                {FormatNumber(ad.ethers)}
                            </TableCell>
                        </TableRow>
                    );
                })}
            </TableBody>
        </Table>
    </TableContainer>
)
}
