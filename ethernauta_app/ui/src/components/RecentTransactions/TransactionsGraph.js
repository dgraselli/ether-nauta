import React from 'react'
import HighchartsReact from 'highcharts-react-official'
import { Grid } from '@material-ui/core';

export default function TransactionsGraph(props) {
    
    
    const options = {
        //chart: { type: 'column' },
        title: {text: props.title},
        xAxis: {
            type: "datetime",
        },
        yAxis: [
            {
                //type: 'logarithmic',
                title: {
                    text: "Ethers",
                },
            },
        ],
        plotOptions: {
            series: {
                marker: {
                    //enabled: true,
                },
            },
        },
        series: props.series,
        chart: {
            type: 'column',
            height: props.heght || 250,
            // events: {
            //     load() {
            //       this.showLoading();
            //       setTimeout(this.hideLoading.bind(this), 2000);
            //     }
        },
    };
    
    return (
        <Grid item xs={12}>
        <HighchartsReact options={{...props.options, ...options}} />
    </Grid>

    )
}
