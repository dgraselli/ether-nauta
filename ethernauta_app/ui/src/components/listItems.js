import React from 'react';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import HelpIcon from '@material-ui/icons/Help';
import DoubleArrowIcon from '@material-ui/icons/DoubleArrow';
import ImportContactsIcon from '@material-ui/icons/ImportContacts';
import InfoIcon from '@material-ui/icons/Info';
import RecordVoiceOverIcon from '@material-ui/icons/RecordVoiceOver';
import ReciptIcon from '@material-ui/icons/Receipt';

import {Divider } from '@material-ui/core';

function ListItemLink(props) {
    
    return <ListItem button component="a" {...props} />;
}

export const mainListItems = (
  <div>
    <ListItemLink href="/">
    <ListItemIcon>
        <InfoIcon />
      </ListItemIcon>
      <ListItemText primary="Informacion" />
    </ListItemLink>

    <ListItemLink href="/help">
    <ListItemIcon>
        <HelpIcon />
      </ListItemIcon>
      <ListItemText primary="Ayuda" />
    </ListItemLink>


    <ListItemLink href='/addresses'>
    <ListItemIcon>
        <ImportContactsIcon />
      </ListItemIcon>
        <ListItemText primary="Analisis de Cuentas" />
    </ListItemLink>

    <ListItemLink href="/last">
    <ListItemIcon>
        <ReciptIcon/>
      </ListItemIcon>
        <ListItemText primary="Analisis Op.Recientes" />
    </ListItemLink>

    <ListItemLink href="/recommendation">
    <ListItemIcon>
        <RecordVoiceOverIcon/>
      </ListItemIcon>
        <ListItemText primary="Recomendaciones" />
    </ListItemLink>


    <Divider style={{marginTop: 40}} />

    <ListItemLink target="_blank" href={(process.env.REACT_APP_MONGO_EXORESS_URL || 'http://localhost:8081')}>
    <ListItemIcon>
        <DoubleArrowIcon />
      </ListItemIcon>
        <ListItemText secondary="Mongo Express" />
    </ListItemLink>

    <ListItemLink target="_blank" href={(process.env.REACT_APP_API_URL + '/a//rtop-10-10' || 'http://localhost:8080/a/rtop-10-10')}>
    <ListItemIcon>
        <DoubleArrowIcon />
      </ListItemIcon>
        <ListItemText secondary="API" />
    </ListItemLink>

    

  </div>
);


