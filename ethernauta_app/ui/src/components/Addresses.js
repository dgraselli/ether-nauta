import React, { useState } from "react";
import {
    Container,
    Grid,
    MenuItem,
    Select,
    Card,
    Dialog,
    DialogTitle,
    DialogContent,
} from "@material-ui/core";
import AddressList from "./Addresses/AddressList";
import ChartGanancias from "./Addresses/ChartGanancias";
import Address from "./Addresses/Address";

export default function Addresses(props) {
    const [topN, setTopN] = useState(10);
    const [tipo, setTipo] = useState("r");
    const [address, setAddress] = useState();

    function SelectTop(props) {
        return (
            <Select
                value={props.value}
                style={{ fontSize: "x-large", color: "blue" }}
                onChange={(e) => setTopN(e.target.value)}
            >
                <MenuItem value={5}>5</MenuItem>
                <MenuItem value={10}>10</MenuItem>
                <MenuItem value={50}>50</MenuItem>
                <MenuItem value={100}>100</MenuItem>
                <MenuItem value={1000}>1000</MenuItem>
            </Select>
        );
    }
    function SelectTipo(props) {
        return (
            <Select
                value={props.value}
                style={{ fontSize: "x-large", color: "blue" }}
                onChange={(e) => setTipo(e.target.value)}
            >
                <MenuItem value={"r"}>Ganancia relativa en % </MenuItem>
                <MenuItem value={"a"}>Ganancia absoluta en U$S </MenuItem>
            </Select>
        );
    }

    return (
        <Grid container alignItems="top" justify="space-around">
            <Grid item xs={12} style={{ textAlign: "center"}}>
                <h1>Cuentas</h1>
                <div style={{ margin: "2px" }}>
                    Contabiliza las ganancias obtenidas en el periodo de
                    analisis y visualiza las X mejores y peores segun: <br/>
                    <b>ganancia absoluta en U$S</b> o {" "}
                    <b>ganancia relativa en %</b> <br />
                    <small>
                        <i>
                            (de aquellas 1000 con mayor ganancia absoluta, para
                            filtrar las que manejan poco margen){" "}
                            Universo de analisis 1.166.313 y 7.540.664 transacciones válidas
                        </i>
                    </small>
                </div>
                <Container style={{ border: "1px solid black" }}>
                    Visualizar las <SelectTop value={topN} /> cuentas con mayor y menor{" "}
                    <SelectTipo value={tipo}/>
                </Container>
            </Grid>
            <Grid item xs={12} lg={6}>
                <ChartGanancias
                    color="green"
                    title="Ganancias"
                    tipo={tipo + "top"}
                    n={topN}
                />
            </Grid>
            <Grid item xs={12} lg={6}>
                <ChartGanancias
                    color="gray"
                    title="Ganancias"
                    tipo={tipo + "bottom"}
                    n={topN}
                />
            </Grid>
            <Grid item xs={12} lg={6}>
                <AddressList
                    onAddressSelected={(e) => setAddress(e)}
                    title="Ganancias superiores"
                    color="green"
                    tipo={tipo + "top"}
                    n={topN}
                />
            </Grid>
            <Grid item xs={12} lg={6}>
                <AddressList
                    onAddressSelected={(e) => setAddress(e)}
                    title="Ganancias inferiores"
                    color="gray"
                    tipo={tipo + "bottom"}
                    n={topN}
                />
            </Grid>
            {address && (
                <Dialog
                    maxWidth="lg"
                    open={address != null}
                    onEscapeKeyDown={() => setAddress(null)}
                >
                    <DialogTitle onClose={() => setAddress(null)}>
                        <b>{address.address}</b>
                        <a
                            target="_blank"
                            rel="noopener noreferrer"
                            href={`https://etherscan.io/address/${address.address}`}
                        >
                            (ver en EtherScan.io)
                        </a>
                    </DialogTitle>
                    <DialogContent>
                        <div style={{ margin: "20px" }}>
                            <div style={{ float: "right" }}></div>
                            <Card p={10}>
                                <Address n={topN} address={address}></Address>
                            </Card>
                        </div>
                    </DialogContent>
                </Dialog>
            )}
        </Grid>
    );
}
