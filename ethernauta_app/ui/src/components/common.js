import React from "react";
import marked from "marked";

export const Markdown = (props) => {
    return (
        <article
            dangerouslySetInnerHTML={{ __html: marked(props.children) }}
        ></article>
    );
};

export const fmt_curreny = new Intl.NumberFormat("es-AR", {
    style: "currency",
    currency: "ARS",
});
export const fmt_curreny_compact = new Intl.NumberFormat("es-AR", {
    style: "currency",
    currency: "ARS",
    notation: "compact",
    compactDisplay: "short",
});
export const fmt_percent = new Intl.NumberFormat("es-AR", {
    style: "percent",
});
export const fmt_number = new Intl.NumberFormat("es-AR", {});
export const fmt_number_compact_short = new Intl.NumberFormat("es-AR", {
    notation: "compact",
    compactDisplay: "short",
});
export const fmt_number_compact_long = new Intl.NumberFormat("es-AR", {
    notation: "compact",
    compactDisplay: "long",
});

export const fmt_date = new Intl.DateTimeFormat("es");

export const FormatPercent = (value) => fmt_percent.format(value);
export const FormatCurrency = (value) => fmt_curreny.format(value);
export const FormatNumber = (value) => fmt_number.format(value);
export const FormatDate = (value) => {
    return new Date(value * 1000).toUTCString();
    //return value;
};

export const FormatMagicNumber = (value) => {
    let v = parseFloat(value);

    let mins = [10 ** 6, 10 ** 3];
    let sufi = [" M", " K"];

    for (let i = 0; i < mins.length; i++) {
        if (Math.abs(v) > mins[i]) return Math.round(v / mins[i]) + sufi[i];
    }

    return fmt_number.format(value);
};

export const FormatMagicCurrency = (value) => {
    let v = parseFloat(value);

    let mins = [10 ** 6, 10 ** 3];
    let sufi = [" G", " M"];
    //let hint = ['Millones']

    for (let i = 0; i < mins.length; i++) {
        if (Math.abs(v) > mins[i]) return Math.round(v / mins[i]) + sufi[i];
    }

    return fmt_curreny.format(value);
};
