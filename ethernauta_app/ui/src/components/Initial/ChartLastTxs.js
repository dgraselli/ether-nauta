import React from "react";
import HighchartsReact from "highcharts-react-official";


export default function ChartLastTxs(props) {
           
        const options = 
        {
            chart: {
                type: 'pie',
                height: props.hight || 250,
            },
            title: {
                text: props.title,
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            accessibility: {
                point: {
                    valueSuffix: '%'
                }
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        format: '<b>{point.name}</b>: {point.percentage:.1f} %'
                    }
                }
            },
            series: [{
                name: 'Addresses',
                colorByPoint: true,
                data: props.data,
            }]
        }
        
        return (
            <HighchartsReact
            options={options}
          />
        );
}


