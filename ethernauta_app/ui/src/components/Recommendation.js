import React, { useState, useEffect } from "react";
import {
    Grid,
    MenuItem,
    Select,
} from "@material-ui/core";
import Axios from "axios";
import RecommendationPanel from "./Recommendation/RecommendationPanel";

export default function Recommendation(props) {
    const [info, setInfo] = useState({});
    const [lastM, setLastM] = useState(60 * 24 * 3);

    const url = process.env.REACT_APP_API_URL;

    const n = props.n || 100;

    useEffect(() => {
        Axios.get(url + "/t/summary_top" + n + "/" + lastM)
            .then((ret) => {
                setInfo(ret.data);
            })
            .catch((ret) => {
                console.log("Error: " + ret);
            });
    }, [url, n, lastM]);

    const SelectTime = (props) => (
        <Select value={lastM} onChange={(e) => setLastM(e.target.value)}>
            <MenuItem value={60 * 24}>el último día</MenuItem>
            <MenuItem value={60 * 24 * 3}>los últimos 3 dias</MenuItem>
            <MenuItem value={60 * 24 * 7}>la última semana</MenuItem>
        </Select>
    );


    return (
        <Grid container alignItems="center">
            <Grid item xs={12} style={{ hight: 40, textAlign: "center" }}>
                <h1>Recomendación</h1>
                <div>
                    <SelectTime />
                </div>
                <Grid item xs={12}>
                    <RecommendationPanel info={info} n={n} />
                </Grid>
            </Grid>
        </Grid>
    );
}
