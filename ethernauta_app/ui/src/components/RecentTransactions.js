import React, { useState, useEffect } from "react";
import { Grid, Select, MenuItem, Container } from "@material-ui/core";
import Axios from "axios";
import ChartPie from "./ChartPie";
import TransactionsGraph from "./RecentTransactions/TransactionsGraph";
import TransactionsList from "./RecentTransactions/TransactionsList";

export default function RecentTransactions(props) {
    const [info, setInfo] = useState({});
    const [data, setData] = useState([]);
    const [transactions, setTransactions] = useState([]);
    const [transactionsByAddress, setTransactionsByAddress] = useState([]);
    const [lastM, setLastM] = useState(60 * 24 * 3);
    const [topN, setTopN] = useState(100);

    const url = process.env.REACT_APP_API_URL;

    const n = topN;


/*     useEffect(() => {
        Axios.get(url + "/t/history_by_address_top" + n + "/" + lastM)
            .then((ret) => {
                setInfo(ret.data);
            })
            .catch((ret) => {
                console.log("Error: " + ret);
            });
    }, [lastM, topN]); */

    useEffect(() => {
        Axios.get(url + "/t/summary_top" + n + "/" + lastM)
            .then((ret) => {
                setInfo(ret.data);
            })
            .catch((ret) => {
                console.log("Error: " + ret);
            });
    }, [lastM, n, url]);

    useEffect(() => {
        Axios.get(url + "/t/history_top" + n + "/" + lastM)
            .then((ret) => {
                setData(ret.data);
            })
            .catch((ret) => {
                console.log("Error: " + ret);
            });
    }, [lastM, n, url]);

    useEffect(() => {
        Axios.get(url + "/t/txs_top" + n + "/" + lastM)
            .then((ret) => {
                setTransactions(ret.data);
            })
            .catch((ret) => {
                console.log("Error: " + ret);
            });
    }, [lastM, n, url]);

    /*
    useEffect(() => {
        Axios.get(url + "/t/history_by_address_top" + n + "/" + lastM)
            .then((ret) => {
                setTransactionsByAddress(ret.data);
            })
            .catch((ret) => {
                console.log("Error: " + ret);
            });
    }, [lastM, n, url]);
    */

    
    const txs_by_a = transactionsByAddress.sort((a, b) => a.ts - b.ts);
    const transactions_by_address = [
        {
            name: "Exitosas",
            color: "blue",
            type: "column",
            data: txs_by_a.filter((x) => x.top).map((x) => [x.ts * 1000, x.e]),
        },
        {
            name: "Resto",
            color: "gray",
            type: "column",
            //yAxis: 1,
            data: txs_by_a.filter((x) => !x.top).map((x) => [x.ts * 1000, x.e]),
        },
    ];
    console.table(transactions_by_address);

    const data_addresses = [
        {
            name: "Cuentas",
            data: [
                {
                    name: "Operaron",
                    color: "blue",
                    y: info.cant_add,
                },
                {
                    name: "resto",
                    y: topN - info.cant_add,
                    color: "silver",
                },
            ],
        },
    ];
    

    const data_ethers = [
        {
            name: "Ethers",
            data: [
                { name: "Compra", y: data.map(x=>x.e_in).reduce((a,b)=>a+b,0), color: "green" },
                { name: "Venta", y: data.map(x=>x.e_out).reduce((a,b)=>a-b,0), color: "red" },
            ],
        },
    ];

     const data_op = [
        {
            name: "# Operaciones",
            data: [
                { name: "Compra", y: data.map(x=>x.op_in).reduce((a,b)=>a+b,0), color: "green" },
                { name: "Venta", y: data.map(x=>x.op_out).reduce((a,b)=>a+b,0), color: "red" },
            ],
        },
    ]; 

/*     const transactions_by_cant = [
        {
            name: "Compras",
            color: "green",
            data: data.map((x) => [x.ts * 1000, x.op_in]),
        },
        {
            name: "ventas",
            color: "red",
            data: data.map((x) => [x.ts * 1000, x.op_out]),
        },
    ]; */

    const transactions_by_ethers = [
        {
            name: "Compras",
            color: "green",
            data: data.map((x) => [x.ts * 1000, x.e_in]),
        },
        {
            name: "ventas",
            color: "red",
            data: data.map((x) => [x.ts * 1000, -x.e_out]),
        },
    ];

    const SelectTime = (props) => (
        <Select
            value={lastM}
            style={{ fontSize: "large", color: "blue" }}
            onChange={(e) => setLastM(e.target.value)}
        >
            <MenuItem value={60 * 24}>el último día</MenuItem>
            <MenuItem value={60 * 24 * 3}>los últimos 3 dias</MenuItem>
            <MenuItem value={60 * 24 * 7}>la última semana</MenuItem>
        </Select>
    );

    function SelectTop() {
        return (
            <Select
                value={topN}
                style={{ fontSize: "x-large", color: "blue" }}
                onChange={(e) => setTopN(e.target.value)}
            >
                <MenuItem value={5}>5</MenuItem>
                <MenuItem value={10}>10</MenuItem>
                <MenuItem value={50}>50</MenuItem>
                <MenuItem value={100}>100</MenuItem>
                <MenuItem value={1000}>1000</MenuItem>
            </Select>
        );
    }

    return (
        <Grid
            container
            spacing={1}
            direction="row"
            justify="center"
            alignItems="flex-start"
        >
            <Grid item xs={12} style={{ hight: 40, textAlign: "center" }}>
                <h1>Ultimas Transacciones</h1>
                <div style={{ margin: "10px" }}>
                    Analiza las operaciones recientes de las cuentas mas
                    exitosas
                    <div>
                        <small>
                            <i>Esto se utiliza para la recomendacion</i>
                        </small>
                    </div>
                </div>

                <Container style={{ border: "1px solid black" }}>
                    Contabilizar las <SelectTop /> mejores cuentas desde{" "}
                    <SelectTime />
                </Container>
            </Grid>

            <Grid item xs={4}>
                <ChartPie
                    title="Cuentas exitosas que operaron"
                    name="op"
                    series={data_addresses}
                ></ChartPie>
            </Grid>
            <Grid item xs={4}>
                <ChartPie
                    title="Operaciones"
                    name="op"
                    series={data_op}
                ></ChartPie>
            </Grid>
            <Grid item xs={4}>
                <ChartPie
                    title="Ethers "
                    name="op"
                    series={data_ethers}
                ></ChartPie>
            </Grid>
            <Grid item xs={12}>
                <TransactionsGraph
                    title="Historial de cantidad ethers en operaciones de cuentas Exitosas por hora"
                    series={transactions_by_ethers}
                />
            </Grid>

            <Grid item xs={8}>
                <TransactionsList
                    title="Operaciones"
                    transactions={transactions}
                />
            </Grid>
        </Grid>
    );
}
