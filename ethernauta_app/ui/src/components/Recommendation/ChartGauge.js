import React from "react";
// Import Highcharts
import Highcharts from "highcharts/highcharts.js";
import highchartsMore from "highcharts/highcharts-more.js";
import solidGauge from "highcharts/modules/solid-gauge.js";
import HighchartsReact from "highcharts-react-official";

highchartsMore(Highcharts);
solidGauge(Highcharts);

export default class ChartGauge extends React.Component {
    render() {
        const options = {
            chart: {
                type: "gauge",
            },

            title: null,

            pane: {
                startAngle: 150,
                endAngle: 30,
            },
            // the value axis
            yAxis: {
                min: -1,
                max: 1,
                labels: { enabled: false },
                lineWidth: 1,

                plotBands: [
                    {
                        from: -1,
                        to: -0.7,
                        color: "red",
                    },
                    {
                        from: -0.7,
                        to: -0.4,
                        color: "#FFB233",
                    },
                    {
                        from: -0.4,
                        to: 0.4,
                        color: "silver", 
                    },
                    {
                        from: 0.4,
                        to: 0.7,
                        color: "#4EFB3D", 
                    },
                    {
                        from: 0.7,
                        to: 1,
                        color: "green",
                    },
                ],
            },

            series: [
                {
                    name: "Señal",
                    data: [this.props.value],
                },
            ],
        };

        return (
            <HighchartsReact
                highcharts={Highcharts}
                options={options}
            />
        );
    }
}
