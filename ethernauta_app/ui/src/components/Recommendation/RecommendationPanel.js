import React, { Fragment } from "react";
import {
    Grid,
    Paper,
} from "@material-ui/core";

import ChartGauge from "./ChartGauge";
import { FormatNumber, FormatPercent } from "../common";
import "./RecommendationPanel.css";

function RecommendationPanel(props) {
    const info = props.info;
    //const n = props.n;

    //const x1 = (info.e_in + info.e_out) / (info.e_in - info.e_out);
    //const x1a = (info.e_in + info.e_out) / (info.e_in - info.e_out);
    const x1b = (info.op_in - info.op_out) / (info.op_in + info.op_out);
    //const x2 = info.cant_add / n;

    //F(x)=1+-1/(x+1)  -> [0 +inf]
    const signal_op = x1b; //1 + -1 / (55 * x1 + 1);

    //const signal = Math.round(100 * signal_intensity * signal_op) / 100;
    const signal = Math.round(100 * signal_op) / 100;
    //const signal_intensity = Math.round(x2 * 100) / 100;

    const operation = signal > 0 ? "COMPRA" : "VENTA";
    const operation_level = Math.abs(signal) > 0.8 ? "FUERTE" : "LEVE";

    const getClass = (c) => {
        switch (c) {
            case "fv":
                return signal <= -0.8 ? "selected" : "normal";
            case "v":
                return signal <= -0.5 && signal > -0.8 ? "selected" : "normal";
            case "-":
                return signal > -0.5 && signal < 0.5 ? "selected" : "normal";
            case "c":
                return signal >= 0.5 && signal < 0.8 ? "selected" : "normal";
            case "fc":
                return signal >= 0.8 ? "selected" : "normal";
            default:
                break;
        }
        return "normal";
    };

    if (!info.cant_add)
        return <div style={{ margin: 50 }}>No hay datos en este periodo</div>;

    return (
        <Fragment>
            <Grid
                container
                spacing={1}
                direction="row"
                justify="center"
                alignItems="center"
                alignContent="center"
            >
                <Grid item xs={12} style={{ textAlign: "center" }}>
                    <br></br>
                    {info.cant_add != null && (
                        <div style={{ paddingTop: 10 }}>
                            <strong
                                style={{ color: "blue", fontSize: "large" }}
                            >
                                {FormatNumber(info.cant_add)}
                            </strong>{" "}
                            de las 100 cuentas mas exitosas operaron ultimamente,
                            inclinandose por una <b>{operation_level}</b>{" "}
                            ventaja en favor de la <b>{operation}</b>
                            <br />
                            <div style={{ fontSize: "small" }}>
                                Realizaron en conjunto
                                <span style={{fontWeight: 'bold', textDecoration: 'underline', color: "green" }}>
                                    {" "}
                                    {info.op_in}
                                </span>{" "}
                                operaciones de compra por{" "}
                                <span style={{ color: "green" }}>
                                    {FormatNumber(info.e_in)} Ethers
                                </span>
                                . y{" "}
                                <span style={{fontWeight: 'bold', textDecoration: 'underline', color: "red" }}>
                                    {info.op_out}
                                </span>{" "}
                                operaciones de venta por{" "}
                                <span style={{ color: "red" }}>
                                    {FormatNumber(-info.e_out)} Ethers
                                </span>
                                <br />
                                Para obtener este indicador se utiliza la
                                diferencia entre cantidad de operaciones de
                                compra y venta. <br />No se basa en la Ethers sino en
                                cantidad de operaciones para evitar que una op
                                grande sesgue el resultado. 
                                <br />
                                Deberia además utilizarse como peso
                                significativo el porcentaje de cuentas que
                                operaron (En este caso {FormatPercent(info.cant_add / 100)})
                            </div>
                        </div>
                    )}
                </Grid>
                <Grid item xs={4}>
                        <ChartGauge value={signal}></ChartGauge>
                </Grid>
                <Grid item xs={4} style={{ padding: 0 }}>
                    <div style={{ color: "green" }} className={getClass("fc")}>
                        Fuerte señal de compra
                    </div>
                    <div style={{ color: "#4EFB3D" }} className={getClass("c")}>
                        Señal de compra
                    </div>
                    <div className={getClass("-")}>Esperar</div>
                    <div style={{ color: "#FFB233" }} className={getClass("v")}>
                        Señal de venta
                    </div>
                    <div className={getClass("fv")}>Fuerte señal de venta</div>
                </Grid>
            </Grid>
        </Fragment>
    );
}

export default RecommendationPanel;
