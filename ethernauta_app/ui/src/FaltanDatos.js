import React from 'react'
import { Grid } from '@material-ui/core'
import { Alert, AlertTitle } from '@material-ui/lab';

export default function FaltanDatos() {
    return (
        <Grid
          container
          spacing={1}
          direction="row"
          justify="center"
          alignItems="center"
          alignContent="stretch"
          wrap="nowrap"
          style={{height: '100%'}}
        >
        <Alert severity="success" variant="outlined">
        <AlertTitle>Debe inicializar la Base de Datos</AlertTitle>
        Ejecute <blockquote> -# bash load_mongo.sh</blockquote>
      </Alert>
          
        </Grid>
    )
}
