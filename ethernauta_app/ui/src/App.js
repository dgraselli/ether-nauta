//import "typeface-roboto";
import React, { useState, useEffect } from "react";
import "bootstrap/dist/css/bootstrap.min.css";

import Main from "./components/Main";
import { BrowserRouter as Router } from "react-router-dom";
import "./App.css";
import Axios from "axios";
import FaltanDatos from "./FaltanDatos";

function App() {
    const [exists, setExists] = useState(false);

    useEffect(() => {

        let link = `${process.env.REACT_APP_API_URL}/data_exists`;
        Axios.get(link)
            .then((res) => {
                setExists(res.data);
            })
            .catch((err) => {
                console.log(err);
            });
    }, []);

    if( ! exists)
    return <FaltanDatos />

    return (
        <Router>
            <Main />
        </Router>
    );
}

export default App;
