# Analisis de la Blockchain de Ethereum 
 Captura de datos para analisis de la cadena de blockchain de Ehtereum.
 > Ethereum es una moneda criptográfica que aprovecha la tecnología blockchain para almacenar transacciones en un libro mayor distribuido.
 > Este libro mayor distribuido es de acceso publico, pero anonimo. Es decir, se conoce de cada cuenta que operaciones realiza, pero no se identifican los tenedores de esas cuentas.
 > A diferencia de Bitcoin, Ethereum gestiona en su blockchain, ademas de transacciones de su monenda Ether, lo que denomina contratos inteligentes. Por lo que existen en su blockchain
   muchas mas transacciones partiulares de cada contratos. De hecho, existen muchas otras monedas asociadas a esos conttatos que se gestionan de esta manera.
 > En este proyecto, solo nos concentramos en las transacciones de Ethers.

## Objetivo
Brindar como servicio mediante suscripcion, la informacion de acciones llevadas a cabo por cuentas exitosas.

Se entiende como cuenta exitosa, aquella que tenga mas redito, de acuerdo a las operaciones que realiza. 
Aquellas cuentas que cuya difrencia entre los recursos obtenidos menos los recursos invretidos sea mayor al resto en un periodo de tiempo acotado. 

> Este trabajo esta vinculado a la materia "Captura y Analisis de datos" de la "Especialista en Inteligencia de Datos orientada a Big Data" de la Universidad Nacional de La Plata (UNLP) 

## Alcance
El alcance inicial del proyecto es lograr identificar las cuentas exitosas y desarrollar un modelo de publicacion a suscriptores a los cuales se les informa las acciones llevadas a cabo por estas cuentas para que puedan si lo desean imitar su comportamiento.
Adicionalmente, y fuera del alcance inicial, se podria realizar analisis de informacion para detectar patrones de comportamiento que permitan identificar, no cuentas exitosas, sino comportamientos exitosos.

## Primeros pasos

Se realizo una captura inicial de 220 MB (contiene las transaccciones de 14hs) con el proposito de comenzar a conocer los datos  con los cuales vamos a trabajar.
El tamaño de la muestra es lo suficientemente grande para contener variedad de informacion, y suficientemente pequeña para poder analizarla sin demasiada demora.

[Catura inicial de datos](https://drive.google.com/file/d/13s9dHn6K481W2I6d_8mDX2YA6_HLcQwk/view?usp=sharing_eil&ts=5e6d0d3a)

### Estimacion de datos

La captura se realizo de las transacciones del ultimo mes. Desde 9470522 hasta 9670527.

**Datos** | **Tamaño**
----------------------------------------- | ------
Captura (9.670.527 - 9.470.522 = 200.005 bloques) | 228 MB
Total  (9.670.527 bloques) | 11.024 MB ~ 11GB

(Sin embargo, la tabla transactions de bigquery tiene alrededor de 293 GB al 20-03.2020)
(Seguramente el tamaño de cada bloque no es homogeneo)

## Datos

### transactions.csv  

   **Column**        |    **Type**     |    **Desc**    |
-----------------|-------------|------|
hash             | hex_string  | Hash of the transaction |
nonce            | bigint      | The number of transactions made by the sender prior to this one |
block_hash       | hex_string  | Hash of the block where this transaction was in |
block_number     | bigint      | Block number where this transaction was in |
transaction_index| bigint      | Integer of the transactions index position in the block |
from_address     | address     | Address of the sender |
to_address       | address     | Address of the receiver. null when its a contract creation transaction |
value            | numeric     | Value transferred in Wei |
gas              | bigint      | Gas provided by the sender <br> *Es la maxima catnidad de gas a pagar (porque pagaria menos??) (Valor tipico 21.000) |
gas_price        | bigint      | Gas price provided by the sender in Wei <br> Es el precio en WEI a pagar. La FEE de la transaccion es gas * gas_price. A mayor valor, mas rapido se ejecuta. (valor tipico de 1 a 10, normal es 4)|
input            | hex_string  | The data sent along with the transaction |
block_timestamp  | bigint      | |


Fuente [blockchain-etl](https://raw.githubusercontent.com/blockchain-etl/ethereum-etl/develop/docs/schema.md)



# Analisis inicial

Se realizo un analisis exploratorio para comprender estructura y semantica de los datos captuados

Hay cuentas que tiene muchisismas (> 100.000) transacciones en el periodo de 30 dias capturado.
Algunas mayoritariamente tienen solo ventas, otras solo ingresos

[Ver analisis de  inicial de datos](analisis.md).




# Precio de las transacciones 

Cada ves que se compra o vende, se paga un pequeño valor (Fee) para que la transaccion sea ejecutada. 
Para este proyecto, consideramos dejar afuera este valor por ser poco significativo y no se tendra en cuenta en los calculos.
El Fee pagado por tx es normalmente es un valor fijo "0.000021 Ethers".  Ose que si uno transfiere 1 Ether (~ $130), pagaria menos de 3 centavos (~$0,027).

Este valor, en principio, hace mas complejo los calculos y el modelo pierde simplicidad, lo cual priorizamos para esta etapa.

En un desarrollo mas avanzado del proyecto, seria preferible tenerlo en cuenta, dado que este valor tiene incidencia en el tiempo de ejecucion de txs y por lo tanto de oportunidades.
Por ej. Las cuentas exitosas pagan un Fee mas alto para ejecutar antes sus txs ?. 



<details>
  <summary>Ampliar información</summary>

> ### Informacion ampliada del Fee
> 
> El precio que se paga por la transaccion (TX) se denomina Fee, y es calculado de la siguiente manera: 
> 
>   **gas * gas_price**
> 
> * El gas_price es el valor que se paga por cada gas 
> * y el gas es la cantidad limite de weis que se esta dispuesto a pagar por una TX.
> 
> Si el costo toma procesar la TX, supera el Fee dispuesto a pagar, la TX se aborta y falla. 
> 
> Supongo que se denomina gas, haciendo una analogia con la gasolina. Asi si uno carga poco "gas", puede que no llegue a destino. Y si el precio de la gasolina "gas_price"
>  es bajo, se supone que es de inferior calidad y se avanza mas lento. 
> 
> De lo que no estoy seguro es de si en caso de poner "de mas", se consume todo, o solo lo necesario para procesar la TX. (En la analogia seria algo asi como que cada viaje comienza con el tanque vacio, o tiene lo que le sobro del viaje anterior.)
> 
> Hay una relacion directa entre el valor que se Fee que se esta dispuesto a pagar, con el tiempo que demorara en procesar la transaccion.
> 
> La web [ethgasstation](https://ethgasstation.info/index.php) se encarga de mostrar en tiempo real, que tiempos promedios existen segun el Fee a pagar.
> 
> [Exlicacion de Gas y Gas Price](https://kb.myetherwallet.com/en/transactions/what-is-gas/)


</details>


## Metodo de evaluación del Scoring

La idea conceptual es la siguiente:

Se evalua **"periodicamente"** mediante algun **"metodo de evaluacion"** el "grado de exito de la cuenta", otorgando a dicha cuenta un **"puntaje"** ( o "score").

Por ejemplo, se evalua mensualmente (*periodo*), la cuentas que mayor redito hayan logrado (*metodo de evaluacion*), otorgando una calificacion a la cuenta (*puntaje*).

Este puntaje, se utiliza para identificar la x mejores cuentas, y ver si se mantienen en puena posicion durante los ultimos x periodos.

Entonces, sabiendo que cuentas tienen reslutados buenos y ademas son consistentes en un lapso de tiempo (x periodos), podemos armar un grupo de cuetnas exitoso.

Luego, viendo que acciones realiza este grupo (compra o venta) de manera mayoritaria y significativa, podemos recomendar imitar dichas acciones.

Ej. El 80% de las cuentas exitosas, compró en el ultimo dia. Y ademas podriamos decir, que % de su capital movio.


A continuacion evaluamos distintos alternativas de 

* Peridicidad
* Metodo de evaluacion
* Puntaje


### Periodicidad

El periodo, podria ser mensual. Consiguiendo asi, mes a mes, los puntajes obtenidos de cada cuenta en cada mes.
Y podriamos reservar los puntajes de los ultimos 12 meses.

* P0 es mes anterior
* P1 es el mes previo a p0
* ...
* Px es el previo al P(x-1)

Evaluar si conviene hacerlo semanalmente

## Puntaje

Este score puede ser :

* real absoluto (ej el redito obtenido) 
* real relativo (ej. posicion en una escala de 0 a 1)
* simbolico (ej. faceta o posicion en rango con resolucion)

## Metodos de evaluacion

Incialmente pensamos en un metodo que tenga en cuenta todos los movimientos desde cierta fecha, al que denominamos "Metodo de ventana activa"

### Metodo 1 (Metodo de ventana activa)

Armamos una cta cte de ethers y de U$S por cada cuenta.

El valor de la transaccion en ethers es positiva si compra y negativa si vende. 

El valor de la transaccion en USS es positiva si vende y negativa si compra. (ethers * precio_vigente * -1)

El puntaje se obtenia asi:

Puntaje de la cuetna X = USS + ETHERS * valor_actual_del_ether

Ventajas:
* Facil 
* Simple de entender

Desventajas
* Es muy sensible al precio actual. 
* Las cuentas que mayormente compran o mayormente venden es el periodo analizado son cambian mucho segun cambie el precio actual.

<details>
  <summary>Ampliar informacion sobre el Metodo 1 de ventana activa</summary>

## Metodo 1 (Metodo de ventana activa)


> Supongamos que armamos una cuenta corriente de la siguiente manera.
> 
> **Fecha** | **Cuenta** | **Valor** | (obs) |
> ----------|------------|-----------|-------|
> Fecha 1 | Cuenta A | 100 |  Compro 100 unidades |
> Fecha 2 | Cuenta A | -20 |  Vendio 20 unidades  |
> Fecha 3 | Cuenta A |  10 |  Compro 20 unidades  |
> Fecha 4 | Cuenta A | -50 |  Vendio 50 unidades  |
>  
> En este punto, sabemos que la Cuenta A ejectuó 4 operaciones y tiene un saldo de 40 unidades.
> 
> Esto sirve para conocer las cuentas con mayor saldo, pero el interes principal es saber cuales cuentas obtuvieron réditos en sus operacoines, no que cuentas tienen mayor cantidad de unidades de alguna criptomoneda (en este caso Ether).
> 
> Por lo que incorporamos a nuesta Cta.Cte. el precio de la criptomoneda al efectuar la oepracion y el costo de la operacion (unidades * valor_unitario).
> Nos quedaria algo asi:
> 
> **Fecha** | **Cuenta**   | **Valor** | **$ unidad** | **$ operacion** | (obs) |
> ----------|--------------|-----------|--------------|-----------------|-------|
> Fecha 1 | Cuenta A | 100 |   $32.01 | $ - 3210.00 |  Compro 100 unidades y gasto $ 3210  |
> Fecha 2 | Cuenta A | -20 |   $35.00 | $    700.00 |  Vendio 20 unidades y recupero $ 700 |
> Fecha 3 | Cuenta A |  10 |   $30.00 | $  - 300.00 |  Compro 10 unidades y gasto $ 300    |
> Fecha 4 | Cuenta A | -50 |   $40.00 | $   2000.00 |  Vendio 20 unidades y recupero $ 2000|
> **TOTAL**   | Cuenta A |  **40** |          | **$   - 810.00**| |
> 
> 
> Entonces, invirtio 810 para quedarse con 40 unidades.
> Si el precio actual de la unidad es $ 40.00, tiene $ 1.600, por lo que su balance es 
> 
> BALANCE = (invertido - recuperado) + tenencia * valor_actual
> 
> BALANCE = - $ 810 + 40 * $40 = - $ 810 + $ 1600 = $ 790
> 
> Balance Positivo en $ 790.
> Este Balance sera el valor de "scoring" para comparar las cuentas y recuperar las mas exitosas en una ventana de tiempo.
> 
> Este sistema, permite obtener la ganancia neta en una ventana de tiempo, ignorando las operaciones previas.
> 
> **No importan las mas ricas (osea, las que más dinero tengan), sino las que más rédito obtuvieron en sus operaciones del último X tiempo.**
> 
> 
> La cuenta calcularse por cuenta por X transaccion
> 
> | **Fecha** | **Cuenta**   | **Valor** | **$ unidad** | **$ operacion** | (obs) |
> |---|----|---|-- |---|---|
> 
> 
> Luego podemos calucular una tabla de scoring por ejemplo diaria, o mensual.
> Llamemosle *T_SCORING_X*, donde X puede ser (ANUAL, MENSUAL, DIARIO, SEMANAL)
> 
> | **Periodo** | **Cuenta**   | **Score** |
> | --- | --- | --- | 
> 
> 
> Donde Score seria el saldo del periodo. O bien podria ser un indice, de la tabla ordenada por saldo descendiente, donde el 1 seria el que mayor redito obtuvo
> y el último es quien mas perdidas obtuvo.

</details>

### Metodo 2 ( Metodo de saldo positivo)

Buscamos entonces un metodo que no dependa tanto del precio actual, para que no intervenga el precio actual.

Por lo tanto no deberiamos no tener en cuenta el saldo de ethers.

Entonces pensamos en trabajar cada transaccion con un saldo de ethers nunca negativo. A este metodo lo denominamos "Metodo de saldo positivo".

Funciona de la sig. manera:

* Con cada taransaccion mantiene un saldo_positivo. Es decir, un saldo que nunca puede ser menor a 0.
* las primeras transacciones que fueran de venta, se descartan (porque darian saldo negativo de ethers)
* Las compras se efectuan por el total de ethers.
* Las ventas por el total siempre y cuando no deje saldo negativo, de lo contrario, permite solo vender lo que tiene.
* Si el saldo en ethers fuera positivo, se genernaria una transaccion ultima, de venta del total del saldo de ethers, asl ultimo valor de transaccion. 
  (Esta evita usar el valor actual del ether.)
 
Revisar Excel con casos, pestaña "Analisis 2"

Ventajas

* No depende del precio actual
* Medianamente simple de interpretar

Desventajas

* Puede dejar afuera cuentas buenas, porque su compra fue apenas anterior al comienzo de la ventana estudio.
* Puede deja afuera cuentas buenas, cuya venta es apenas posterior a la ventana de estudio por valores mejores al ultimo valor.
 


### Metodo 3 (Metodo de diferencia en ventas)

Otro metodo que puede ser util, es tomar solo aquellas cuentas que tuvieron reditos de las ventas.

Este metodo, denominado "Metodo de diferencia en ventas" se basa unicamente en el resultado obtenido de las ventas. 

Funciona de la siguiente manera:

* Con cada taransaccion mantiene un saldo_positivo. Es decir, un saldo que nunca puede ser menor a 0.
* las primeras transacciones que fueran de venta, se descartan (porque darian saldo negativo de ethers)
* Las compras se efectuan por el total de ethers. 
* Con cada compra, lleva cuenta del valor promedio de compra. 
* Las ventas por el total siempre y cuando no deje saldo negativo, de lo contrario, permite solo vender lo que tiene.
* Al momento de la venta, calcula la diferencia que obtuvo respecto al valor promedio. Esta diferencia puede ser positiva o negativa.
* Si queda saldo en ethers o no, no se tiene en cuenta ( a diferencia del metodo 1 y 2)
* Finalimente, el score se calcula como la sumatoria de las diferencias que obtuvo en todas las ventas.

Ventajas

* No depende del precio actual
* Medianamente simple de interpretar
* Se basea unicamente en cuentas que operan para sacar redito a plazo cierto (medio o largo de acuerdo a la ventana de estudio)

Desventajas

* Puede dejar afuera cuentas buenas, porque su compra fue apenas anterior al comienzo de la ventana estudio.
* Puede deja afuera cuentas buenas, cuya venta es apenas posterior a la ventana de estudio.


<details>
  <summary>Ampliar informacion sobre el Metodo 3</summary>

> ##  Metodo 3 (Metodo de diferencia en ventas)
> 
**Fecha** | **Cuenta** | **Ethers**   | **Valor** | **Saldo positivo**| **valor operable** | **obs** |
----------|------------|--------------|-----------|-------------------|--------------------|-------|
Fecha 1 | Cuenta A     |  100         |   $32.01  |  100              | 100                |  Compro 100 unidades a $32 (valor promeido $32)  |
Fecha 2 | Cuenta A     | - 20         |   $35.00  |   80              | -20                |  Vendio 20 unidades a $35. ( 20*(35-32) = +60) Gano +60 |
Fecha 3 | Cuenta A     |   10         |   $30.00  |   90              |  10                |  Compro 10 unidades a $30  (valor promedio (80*32 + 10*30) / (80+10) = 31,77   |
Fecha 4 | Cuenta A     | -200         |   $20.00  |    0              | -90                |  Vendio 90 unidades a 40 (90 *(20-31,77) = -1059,3 ) Perdio -1059 |
**TOTAL**   | Cuenta A |              |           |                   |                    |  −999,3 (perdio 999,3) |
> 
> En resumen, esta cuenta perdio 999,3 en las diferenicas por ventas que realizo.


</details>


## Datos adicioneales de cuentas

Con el proposito de realizar luego, analisis explorativo de las cuentas, se propone agregar a la tabla de scoring la sig. info:

|**Dato** | **Descripcion** |
| :---: | :---- |
|born | Fecha en de la primer transaccion. Con esto podemos analizar cuentas nuevas vs antiguas |
|tx_time_media | Esperanza del tiempo entre transacciones |
|tx_time_var | Varianza del tiempo entre transacciones |
|? | Ampliar, ej.  primera y ultima venta o compra |





### Links
* [BiqQuery](https://console.cloud.google.com/bigquery?sq=21854208155:08e01b51319a4ddcbb9826a98f3c6413) 
* [Kaggle notebook](https://www.kaggle.com/dgraselli/ethernauta-queries)
* [awesome-bigquery-views](https://github.com/RokoMijic/awesome-bigquery-views)
* [Goggle Big Query relacionada a Ethereum](https://console.cloud.google.com/marketplace/details/ethereum/crypto-ethereum-blockchain?pli=1)
* [Problamatica al Calcular Saldos](https://medium.com/google-cloud/how-to-query-balances-for-all-ethereum-addresses-in-bigquery-fb594e4034a7)
* [Sistema de recomendaciones](https://medium.com/google-cloud/building-token-recommender-in-google-cloud-platform-1be5a54698eb)
* [Kaggle Ethereum DATA](https://www.kaggle.com/bigquery/ethereum-blockchain)
* [Coinfi](https://www.coinfi.com/) Sitio de seguimiento de Criptomonedas por el creador de ethereum-etl Evgeny Medvedev (Mas de 2000 criptos ?!?!?)
* Chequear esta [Aragoon](https://mainnet.aragon.org/)
* [Pub/Sub bigQueyr tables at Google](https://medium.com/google-cloud/live-ethereum-and-bitcoin-data-in-google-bigquery-and-pub-sub-765b71cd57b5) Excelente articulo de Evgeny Medvedev para suscribir a eventos si queremos tener un sistema en linea. 
* Idem previo [real-time-ethereum-notifications-for-everyone-for-free](https://medium.com/google-cloud/real-time-ethereum-notifications-for-everyone-for-free-a76e72e45026)
* [Gini Calc](http://shlegeris.com/gini)
* [Google Datastudio Dashboard](https://datastudio.google.com/u/0/reporting/197An_pAkoJ_fAKCRTY6GO608j2mwz8CO/page/yJtg)

Mongo
* https://www.adictosaltrabajo.com/2013/11/20/primeros-pasos-mongodb/
* https://docs.mongodb.com/manual/core/map-reduce/

### Consultas

* Si algiuen se registra en un broker para operar en Ether, el broker crea una cartera individual para la persiona en ether? Es decir, crea una address nueva, u utiliza la address del brocker e internamente administra las tenencias de cada cliente ?
* El Fee que figura en transactions (gas * gas_price / 10^18) es el total que paga ? No siempre coincide con etherscan.io
* Falta indicar las transacciones fallidas. Ahora solo filtra por value > 0. En Kaggle, figura el dato receipt_status=1 Ok 0=Fail. Convendra utilizar "traces" en lugar de "transactions". 
  [Ver este articulo](https://wiki.parity.io/JSONRPC-trace-module) 
* Que es el Unle Rate ?? https://etherchain.org/ 
