# Analisis de Datos

## Cuentas con mas cantidad de tx

En el periodo 



-----------------------------------------------------------------------------------------
| address                                    | e_in     | e_out    | cant_op | Ref | Obs |
| ------------------------------------------ | -------- | -------- | ------- |-----|-----|
| 0xea674fdde714fd979de3edf0f56aa9716b898ec8 | 441607.0 | 4.0      | 441611  | https://ethermine.org/ | Pool de mineros |
| 0x52bc44d5378309ee2abf1539bf71de1b7d7be3b5 | 191142.0 | 6.0      | 191148  | https://nanopool.org/ | Pool de mineros |
| 0x3f5ce5fbfe3e9af3971dd833d26ba9b5c936f0be | 31085.0  | 126987.0 | 158072  | https://www.binance.com/en | Exchange ? |
| 0xbcf935d206ca32929e1b887a07ed240f0d8ccd22 | 0.0      | 129107.0 | 129107  | https://million.money/ | - |
| 0x8fd00f170fdf3772c5ebdcd90bf257316c69ba45 | 94989.0  | 1476.0   | 96465   | - | - |
| 0x683b8615d6099a71941cd693c151ef7c0573fb44 | 2.0      | 95885.0  | 95887   | - | - |
| 0xcfcf469b76113c5e7218f8275eaf10a881ac106d | 42635.0  | 44477.0  | 87112   | - | - |
| 0x7c06a6b2e57593daa0040b0fbc2a9e0ff8fed0d5 | 85849.0  | 6.0      | 85855   | - | - |
| 0x829bd824b016326a401d083b33d092293333a830 | 85771.0  | 0.0      | 85771   | https://www.f2pool.com/ | ? |
--------------------------------------------------------------------------------------

