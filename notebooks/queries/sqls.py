txs_top100 = """
select date_trunc('day',to_timestamp(ts)) as fecha, sum(ethers) as ethers, sum(case when ethers > 0 then 1 else -1 end) as cant 
from ctacte 
where address in (select address  from scoring_m3_pg order by diff desc limit 100)
group by 1
order by 1
"""

txs_bottom100 = """
select date_trunc('day',to_timestamp(ts)) as fecha, sum(ethers) as ethers, sum(case when ethers > 0 then 1 else -1 end) as cant 
from ctacte 
where address in (select address  from scoring_m3_pg order by diff asc limit 100)
group by 1
order by 1"""

prices = "select snapshottime as fecha, highprice_ask as price from prices where snapshottime > '2020-02-12'"

