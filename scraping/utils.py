import os


def get_all_csv_from_dir(dir_base, files):
    data = os.listdir(dir_base)
    for x in data:
        if os.path.isdir(dir_base + "/" + x):
            get_all_csv_from_dir(dir_base + "/" + x)
        else:
            if ".csv" in x:
                files.append(dir_base + "/" + x)
