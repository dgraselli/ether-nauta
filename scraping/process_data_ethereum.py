import pandas as pd
from .utils import get_all_csv_from_dir
path_transactions = "transactions"

files_names_transactions = []
get_all_csv_from_dir(path_transactions, files_names_transactions)

path_receipts = "receipts"
files_names_receipts = []
get_all_csv_from_dir(path_receipts, files_names_receipts)

paths_files = []
for filename_receipt in files_names_receipts:
    number = filename_receipt.split("_")[-1]
    for filename_tx in files_names_transactions:
        if number in filename_tx:
            paths_files.append({"receipt_file": filename_receipt, "tx_file": filename_tx})

dataframes = []
for f in paths_files:
    df_tx = pd.read_csv(f["tx_file"])
    df_receipt = pd.read_csv(f["receipt_file"])
    df_tx = df_tx.drop(
        [
            "nonce",
            "block_hash",
            "block_number",
            "transaction_index",
            "input"],
        axis=True,

    )
    df_receipt = df_receipt.drop(
        [
            "transaction_index",
            "block_hash",
            "block_number",
            "root"

        ],
        axis=True)
    df_receipt["hash"] = df_receipt["transaction_hash"]
    df_receipt = df_receipt.drop("transaction_hash", axis=True)
    dataframes.append({"r": df_receipt, "tx":df_tx})

dataframes_union = []
for d in dataframes:
    dataframes_union.append(
        pd.merge(d["tx"], d["r"], how="left", on="hash")
    )
result = pd.concat(dataframes_union)
result.to_csv("tx_with_receipt_20200321.csv", index=False)
