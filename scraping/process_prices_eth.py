import pandas as pd
from utils import get_all_csv_from_dir
from datetime import datetime
import time


files_csv = []
path_files = "prices_eth_data"
get_all_csv_from_dir(path_files, files_csv)

dataframes = []

for f in files_csv:
    dataframes.append(pd.read_csv(f))
result = pd.concat(dataframes)
result.to_csv("prices_eth_usd_20190317_20200316_v3.csv", index=0)

snapshotTime = result.sort_values(by='snapshotTime')["snapshotTime"]
times = snapshotTime.array
first = times[0]
last = times[-1]


def str_date_to_timestamp(str_date):
    return time.mktime(datetime.strptime(first, "%Y/%m/%d %H:%M:%S").timetuple())


def next_5m_date(str_date):
    timestamp_date = str_date_to_timestamp(str_date)
    next_timestamp_date = timestamp_date + 300# seconds
    d = datetime.fromtimestamp(next_timestamp_date)
    return d.strftime("%Y/%m/%d %H:%M:%S")

df = result
df = df.sort_values(by='snapshotTime')
end = 0
while not end:
    next_v = next_5m_date(first)
    r = df.loc[(df['snapshotTime'] == next_v)]
    if r.empty:
        b = df.loc[(df['snapshotTime'] == first)]
        b["snapshotTime"] = next_v
        df = df.append(b, ignore_index=True)
    first = next_v
    print(first, last)
    if first == last:
        end = 1
df = df.sort_values(by='snapshotTime')
df.to_csv("prices_eth_usd_20190317_20200316_v3.csv", index=0)
