import csv
import json
import requests
import time
from datetime import datetime, timedelta

API_KEYS_ENABLED = []
API_KEYS_DISABLED = []

#ref: https://labs.ig.com

API_KEY = ""
URL_LOGIN = "https://api.ig.com/gateway/deal/session"
USER = ""
PASSWD = ""

data_login = {
    "identifier": USER,
    "password": PASSWD
}


ERROR_CHANGE_API_KEY = "error.public-api.exceeded-account-historical-data-allowance"
ETH = "CS.D.ETHUSD.CFD.IP"

RESOLUTIONS = {
    "S": "SECONDS",
    "M1": "MINUTE",
    "M2": "MINUTE_2",
    "M3": "MINUTE_3",
    "M5": "MINUTE_5",
    "M10": "MINUTE_10",
    "M15": "MINUTE_15",
    "M30": "MINUTE_30",
    "H1": "HOUR",
    "H2": "HOUR_2",
    "H4": "HOUR_4",
    "D": "DAY",
    "W": "WEEK"
}

URL_PRICES = "https://api.ig.com/gateway/deal/prices/{instrument}?resolution={resolution}&from={from}&to={to}&max=10&pageSize=99999999"

class IG:
    def __init__(self, instrument=ETH, resolution=RESOLUTIONS["M5"]):
        self.API_KEYS_ENABLED = API_KEYS_ENABLED
        self.API_KEYS_DISABLED = API_KEYS_DISABLED
        self.instrument = instrument
        self.last_api_key_used = self.API_KEYS_ENABLED[0]
        self.resolution = resolution
        self.fieldnames = [
            'snapshotTime',
            'openPrice_bid',
            'openPrice_ask',
            'closePrice_bid',
            'closePrice_ask',
            'highPrice_bid',
            'highPrice_ask',
            'lowPrice_bid',
            'lowPrice_ask',
            'lastTradedVolume'
        ]

    def login(self):
        self.set_headers_auth()
        response = requests.post(url=URL_LOGIN, data=json.dumps(data_login), headers=self.headers_auth)
        if response.status_code == 200:
            self.headers = self.headers_auth.copy()
            self.headers["X-SECURITY-TOKEN"] = response.headers["X-SECURITY-TOKEN"]
            self.headers["CST"] = response.headers["CST"]
            self.headers["version"] = "3"
        else:
            raise Exception("Not login:{} {}".format(response.text, response.status_code))

    def set_headers_auth(self):
        self.headers_auth = {

            'X-IG-API-KEY': self.last_api_key_used,
            'content-type': 'application/json; charset=UTF-8',
            'version': '2'
        }

    def send_post(self, from_date, to_date):
        data_url = {
            "instrument": self.instrument,
            "resolution": self.resolution,
            "from": self.get_from(from_date),
            "to": self.get_to(to_date)
        }
        prices_response = requests.get(
            url=URL_PRICES.format(**data_url),
            headers=self.headers
        )
        return prices_response

    def get_data_of_instrument(self, from_date, to_date):
        prices_response = self.send_post(from_date, to_date)
        while prices_response.status_code == 403 and prices_response.json()["errorCode"] == ERROR_CHANGE_API_KEY:
            self.change_api_key()
            prices_response = self.send_post(from_date, to_date)
        return prices_response

    def change_api_key(self):
        self.API_KEYS_ENABLED.remove(self.last_api_key_used)
        self.API_KEYS_DISABLED.append(self.last_api_key_used)
        if not self.API_KEYS_ENABLED:
            raise Exception("No more api keys")
        self.last_api_key_used = self.API_KEYS_ENABLED[0]
        self.login()

    def get_from(self, datetime_from):
        #return datetime_from.strftime("%Y-%m-%dT%H:%M:%S")
        return str(datetime_from).split(" ")[0] + "T00:00:00"

    def get_to(self, datetime_to):
        #return datetime_to.strftime("%Y-%m-%dT%H:%M:%S")
        return str(datetime_to).split(" ")[0] + "T23:59:59"

    def generate_csv_from_date_for_days(self, from_date, timedelta_in_days, init_file=True):
        with open(NAME_FILE_TO_SAVE, mode='a') as csv_file:
            if init_file:
                writer = csv.DictWriter(csv_file, fieldnames=self.fieldnames)
                writer.writeheader()
            for day in range(timedelta_in_days):
                prices_response = self.get_data_of_instrument(
                    from_date,
                    from_date,

                )
                try:
                    for price in prices_response.json()["prices"]:
                        row = {
                            "snapshotTime": price["snapshotTime"],
                            "openPrice_bid": price["openPrice"]["bid"],
                            "openPrice_ask": price["openPrice"]["ask"],
                            "closePrice_bid": price["closePrice"]["bid"],
                            "closePrice_ask": price["closePrice"]["ask"],
                            "highPrice_bid": price["highPrice"]["bid"],
                            "highPrice_ask": price["highPrice"]["ask"],
                            "lowPrice_bid": price["lowPrice"]["bid"],
                            "lowPrice_ask": price["lowPrice"]["ask"],
                            "lastTradedVolume": price["lastTradedVolume"]
                        }
                        writer.writerow(row)
                except KeyError as k:
                    print("KeyError: " + str(k))
                    print(price)

                print("zzz...", from_date)
                from_date = from_date + timedelta(days=1)
                time.sleep(5)
                print("action..")


    def generate_csv_from_dict(self, prices, init_file=True):
        with open('test.csv', mode='a') as csv_file:
            if init_file:
                writer = csv.DictWriter(csv_file, fieldnames=self.fieldnames)
                writer.writeheader()

            try:
                for price in prices["prices"]:
                    row = {
                        "snapshotTime": price["snapshotTime"],
                        "openPrice_bid": price["openPrice"]["bid"],
                        "openPrice_ask": price["openPrice"]["ask"],
                        "closePrice_bid": price["closePrice"]["bid"],
                        "closePrice_ask": price["closePrice"]["ask"],
                        "highPrice_bid": price["highPrice"]["bid"],
                        "highPrice_ask": price["highPrice"]["ask"],
                        "lowPrice_bid": price["lowPrice"]["bid"],
                        "lowPrice_ask": price["lowPrice"]["ask"],
                        "lastTradedVolume": price["lastTradedVolume"]
                    }
                    writer.writerow(row)
            except KeyError as k:
                print("KeyError: " + str(k))
                print(price)

    def get_price(self, prices_response):
        price = prices_response.json()["prices"][-1:][0]
        row = {
            "snapshotTime": price["snapshotTime"],
            "openPrice_bid": price["openPrice"]["bid"],
            "openPrice_ask": price["openPrice"]["ask"],
            "closePrice_bid": price["closePrice"]["bid"],
            "closePrice_ask": price["closePrice"]["ask"],
            "highPrice_bid": price["highPrice"]["bid"],
            "highPrice_ask": price["highPrice"]["ask"],
            "lowPrice_bid": price["lowPrice"]["bid"],
            "lowPrice_ask": price["lowPrice"]["ask"],
            "lastTradedVolume": price["lastTradedVolume"]
        }
        return row

    def generate_data(self, from_date, to_date):
        prices_response = self.get_data_of_instrument(
            from_date,
            to_date,

        )
        return self.get_price(prices_response)

    def get_data_today(self, from_date=None, to_date=None):
        if not from_date:
            from_date = datetime.now() + timedelta(hours=4)
        if not to_date:
            to_date = from_date
        prices_response = self.get_data_of_instrument(
            from_date,
            to_date,

        )
        return prices_response.json()["prices"]



NAME_FILE_TO_SAVE = ""
ig = IG()
ig.login()
ig.generate_csv_from_date_for_days(datetime(2019, 2, 1), 28)
