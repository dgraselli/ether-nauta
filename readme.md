# TP de la materia Captura y Almacenamiento de Información de la Esp. Inteligencia de datos orientada a Big-Data 

## Objetivo
  Poder identificar las cuentas que realicen mejores operaciones de compra/venta de la criptomoneda ether, mediante un estrategia de scoreo

## Definición del dominio e hipótesis propia:
Ether es una criptomoneda mediante la cual se opera sobre una blockchain pública propia llamada [Ethereum](https://es.wikipedia.org/wiki/Ethereum). Sobre esta red se realizan diversas operaciones (transacciones) entre 2 cuentas (address). Estas 2 cuentas pueden ser 2 wallets o smart contracts. Una transacción entonces va a estar compuesta de un emisor (**from_address**) y un receptor (**to_address**) y un mensaje a enviar. Este mensaje puede ser de distintos tipos si el receptor es un smart contract. Desde el receptor al emisor se puede enviar ether.

###### Asumimos que:

si ambas cuentas no son smart contracts, son wallets y además
  que si la transacción que se está efectuando desde la cuenta "A" (**from_address**) a la cuenta "B" (**to_address**), tiene valor de ether >0, se está realizando el envío de ether, entonces la cuenta "A"(**from_address**) le está vendiendo ether a la cuenta "B"(**to_address**) y obviamente la cuenta "B" le está comprando ether a la cuenta "A".

 Dado que las transacciones dentro de la blockchain no tienen cotizaciones no tenemos valores de ninguna moneda [fiat](https://es.wikipedia.org/wiki/Dinero_por_decreto)

  Ej de una [transacción](https://etherscan.io/tx/0x9df9a0662f1735ea65bab86f04e0a3cda7ae71d3b2931b5755b65a0bd3a8d9c9)
  
<img src="https://gitlab.com/dgraselli/ether-nauta/-/raw/re_edit_readme/imgs/tx_eth.png" width="800" height="400" />


## Captura de datos iniciales y almacenamiento:

### Datos a necesitar:

Necesitamos en set de datos de las transacciones en la blockchain de ethereum y  otro set de datos de las cotizaciones de la criptomoneda en alguna moneda fiat en algún broker/exchange donde se realicen compra/venta de ether.

### Captura de transacciones: 

 Obtención de un mes de transacciones: Para obtener una porción de datos de un mes de la blockchain se utilizó la  herramienta https://github.com/blockchain-etl/ethereum-etl y se extrajo transacciones desde el bloque:9470522 hasta el bloque:9679527. Como la herramienta devuelve un set de datos particionado en subdirectorios se realizó un script en python([ process_data_ethereum.py](https://gitlab.com/dgraselli/ether-nauta/-/blob/re_edit_readme/scraping/process_data_ethereum.py)) para unificación de  todos los .cvs  en 1 solo y realizar una primer  limpieza de datos. Esta limpieza consistió en quedarnos en este nuevo set de datos unificado con los campos que necesitamos para poder realizar el objetivo, estos campos son:
 
  - hash: id de la transaccion
  - from_address: quien realiza la transacción
  - to_address: quien recibe la transacción
  - value: valor de ETH enviado 
  - block_timestamp: marca de tiempo de confirmación de la transacción en la blockchain, es decir el momento de que dicha transacción quedó asentada en blockchain
  - contract_address: indica si es o no un smart contract
  - status: indica si la transaccion fallo o fue satisfactoria
  - gas: este campo hace referencia al pago del uso de la red, que no será expuesto en este trabajo en esta versión
  - gas_price: idem anterior
  - cumulative_gas_used:idem anterior
  - gas_used:idem anterior

### Captura de cotizaciones:
 
  Para efectuar la captura de cotizaciones  de ether, se realizó un script de scraping sobre el broker https://www.ig.com/esp a traves de su api: https://labs.ig.com.
  Se utilizo el siguiente script: [cliente](https://gitlab.com/dgraselli/ether-nauta/-/blob/master/scraping/client_broker_ig.py) obteniendo las cotizaciones en una ventana de 5 minutos. Este script por condiciontes de la api del broker, permite obtener datos por bloques de 30 días, por lo tanto se realizó la consolidación de estos csvs con el siguiente [script]( https://gitlab.com/dgraselli/ether-nauta/-/blob/master/scraping/process_prices_eth.py)

Las cotizaciones extraidas estan valorizadas en USD. El set de datos de las cotizaciones de ETH/USD tiene los siguientes campos:

  - snapshotTime 
  - openPrice_bid
  - openPrice_ask
  - closePrice_bid
  - closePrice_ask
  - highPrice_bid
  - highPrice_ask
  - lowPrice_bid
  - lowPrice_ask
  - lastTradedVolume


### Set de datos resultantes: 
 
* [transacciones](https://drive.google.com/file/d/1hqADdU8AlR7ayABYLsi0TnyAsibnyLQa/view?ts=5e76a7e9) aprox. 21.5M rows - 4.44Gb

* [cotizaciones](https://drive.google.com/file/d/1WC0y1sEmybvx3Mk7cLjF4uU0g-9TPMxt/view?usp=sharing)


## Analisis inicial

Se realizo un analisis exploratorio para comprender estructura y semantica de los datos capturados

Hay cuentas que tiene muchisismas (> 100.000) transacciones en el periodo de 30 dias analizado.
Algunas mayoritariamente tienen solo ventas, otras solo ingresos.

[Ver analisis de  inicial de datos](analisis.md).

### Precio de las transacciones

Cada vez que se compra o vende, se paga un pequeño valor (Fee) para que la transaccion sea ejecutada.
Para este proyecto, consideramos dejar afuera este valor por ser poco significativo y no se tendra en cuenta en los calculos.
El Fee pagado por tx es normalmente es un valor fijo "0.000021 Ethers".  O sea que si uno transfiere 1 Ether (~ $130), pagaria menos de 3 centavos (~$0,027).

Este valor, en principio, hace mas complejo los calculos y el modelo pierde simplicidad, lo cual priorizamos para esta etapa.

En un desarrollo mas avanzado del proyecto, seria preferible tenerlo en cuenta, dado que este valor tiene incidencia en el tiempo de ejecucion de txs y por lo tanto en aprovechar oportunidades a tiempo.
Por ej. Las cuentas que pagan un Fee mas alto alientan a los mineros a procesar su transaccion antes, por lo que son priorizadas sobre otras.

<details>
  <summary>Ampliar información</summary>

> ### Informacion ampliada del Fee
> 
> El precio que se paga por la transaccion (TX) se denomina Fee, y es calculado de la siguiente manera: 
> 
>   **gas * gas_price**
> 
> * El gas_price es el valor que se paga por cada gas 
> * y el gas es la cantidad limite de weis que se esta dispuesto a pagar por una TX.
> 
> Si el costo toma procesar la TX, supera el Fee dispuesto a pagar, la TX se aborta y falla. 
> 
> Supongo que se denomina gas, haciendo una analogia con la gasolina. Asi si uno carga poco "gas", puede que no llegue a destino. Y si el precio de la gasolina "gas_price"
>  es bajo, se supone que es de inferior calidad y se avanza mas lento. 
> 
> De lo que no estoy seguro es de si en caso de poner "de mas", se consume todo, o solo lo necesario para procesar la TX. (En la analogia seria algo asi como que cada viaje comienza con el tanque vacio, o tiene lo que le sobro del viaje anterior.)
> 
> Hay una relacion directa entre el valor que se Fee que se esta dispuesto a pagar, con el tiempo que demorara en procesar la transaccion.
> 
> La web [ethgasstation](https://ethgasstation.info/index.php) se encarga de mostrar en tiempo real, que tiempos promedios existen segun el Fee a pagar.
> 
> [Exlicacion de Gas y Gas Price](https://kb.myetherwallet.com/en/transactions/what-is-gas/)

</details>

## Método de evaluación del Scoring

La idea conceptual es la siguiente:

Se evalua **"periodicamente"** mediante algun **"metodo de evaluacion"** el "grado de exito de la cuenta", otorgando a dicha cuenta un **"puntaje"** ( o "score").

Por ejemplo, se evalua mensualmente (*periodo*), la cuentas que mayor redito hayan logrado (*metodo de evaluacion*), otorgando una calificacion a la cuenta (*puntaje*).

Este puntaje, se utiliza para identificar la x mejores cuentas, y ver si se mantienen en puena posicion durante los ultimos x periodos.

Entonces, sabiendo que cuentas tienen reslutados buenos y ademas son consistentes en un lapso de tiempo (x periodos), podemos armar un grupo de cuentas exitoso.

Luego, viendo que acciones realiza este grupo (compra o venta) de manera mayoritaria y significativa, podemos recomendar imitar dichas acciones.

Ej. El 80% de las cuentas exitosas, compró en el ultimo dia. Y ademas podriamos decir, que % de su capital movio.

A continuacion evaluamos distintos alternativas de 

* Peridicidad
* Método de evaluacion
* Puntaje

### Periodicidad

El periodo, podria ser mensual. Consiguiendo asi, mes a mes, los puntajes obtenidos de cada cuenta en cada mes.
Y podriamos reservar los puntajes de los ultimos 12 meses.

* P0 es mes anterior
* P1 es el mes previo a p0
* ...
* Px es el previo al P(x-1)

Evaluar si conviene hacerlo semanalmente

### Puntaje

Este score puede ser :

* real absoluto (ej el redito obtenido) 
* real relativo (ej. posicion en una escala de 0 a 1)
* simbolico (ej. faceta o posicion en rango con resolucion)

### Métodos de evaluacion

Incialmente pensamos en un metodo que tenga en cuenta todos los movimientos desde cierta fecha, al que denominamos "Metodo de ventana activa"

#### Método 1 (Método de ventana activa)

Armamos una cta cte de ethers y de U$S por cada cuenta.

El valor de la transaccion en ethers es positiva si compra y negativa si vende. 

El valor de la transaccion en USS es positiva si vende y negativa si compra. (ethers * precio_vigente * -1)

El puntaje se obtenia asi:

Puntaje de la cuetna X = USS + ETHERS * valor_actual_del_ether

Ventajas:
* Facil 
* Simple de entender

Desventajas
* Es muy sensible al precio actual. 
* Las cuentas que mayormente compran o mayormente venden es el periodo analizado son cambian mucho segun cambie el precio actual.

<details>
  <summary>Ampliar informacion sobre el Metodo 1 de ventana activa</summary>

#### Método 1 (Método de ventana activa)


> Supongamos que armamos una cuenta corriente de la siguiente manera.
> 
> **Fecha** | **Cuenta** | **Valor** | (obs) |
> ----------|------------|-----------|-------|
> Fecha 1 | Cuenta A | 100 |  Compro 100 unidades |
> Fecha 2 | Cuenta A | -20 |  Vendio 20 unidades  |
> Fecha 3 | Cuenta A |  10 |  Compro 20 unidades  |
> Fecha 4 | Cuenta A | -50 |  Vendio 50 unidades  |
>  
> En este punto, sabemos que la Cuenta A ejectuó 4 operaciones y tiene un saldo de 40 unidades.
> 
> Esto sirve para conocer las cuentas con mayor saldo, pero el interes principal es saber cuales cuentas obtuvieron réditos en sus operacoines, no que cuentas tienen mayor cantidad de unidades de alguna criptomoneda (en este caso Ether).
> 
> Por lo que incorporamos a nuesta Cta.Cte. el precio de la criptomoneda al efectuar la oepracion y el costo de la operacion (unidades * valor_unitario).
> Nos quedaria algo asi:
> 
> **Fecha** | **Cuenta**   | **Valor** | **$ unidad** | **$ operacion** | (obs) |
> ----------|--------------|-----------|--------------|-----------------|-------|
> Fecha 1 | Cuenta A | 100 |   $32.01 | $ - 3210.00 |  Compro 100 unidades y gasto $ 3210  |
> Fecha 2 | Cuenta A | -20 |   $35.00 | $    700.00 |  Vendio 20 unidades y recupero $ 700 |
> Fecha 3 | Cuenta A |  10 |   $30.00 | $  - 300.00 |  Compro 10 unidades y gasto $ 300    |
> Fecha 4 | Cuenta A | -50 |   $40.00 | $   2000.00 |  Vendio 20 unidades y recupero $ 2000|
> **TOTAL**   | Cuenta A |  **40** |          | **$   - 810.00**| |
> 
> 
> Entonces, invirtio 810 para quedarse con 40 unidades.
> Si el precio actual de la unidad es $ 40.00, tiene $ 1.600, por lo que su balance es 
> 
> BALANCE = (invertido - recuperado) + tenencia * valor_actual
> 
> BALANCE = - $ 810 + 40 * $40 = - $ 810 + $ 1600 = $ 790
> 
> Balance Positivo en $ 790.
> Este Balance sera el valor de "scoring" para comparar las cuentas y recuperar las mas exitosas en una ventana de tiempo.
> 
> Este sistema, permite obtener la ganancia neta en una ventana de tiempo, ignorando las operaciones previas.
> 
> **No importan las mas ricas (osea, las que más dinero tengan), sino las que más rédito obtuvieron en sus operaciones del último X tiempo.**
> 
> 
> La cuenta calcularse por cuenta por X transaccion
> 
> | **Fecha** | **Cuenta**   | **Valor** | **$ unidad** | **$ operacion** | (obs) |
> |---|----|---|-- |---|---|
> 
> 
> Luego podemos calucular una tabla de scoring por ejemplo diaria, o mensual.
> Llamemosle *T_SCORING_X*, donde X puede ser (ANUAL, MENSUAL, DIARIO, SEMANAL)
> 
> | **Periodo** | **Cuenta**   | **Score** |
> | --- | --- | --- | 
> 
> 
> Donde Score seria el saldo del periodo. O bien podria ser un indice, de la tabla ordenada por saldo descendiente, donde el 1 seria el que mayor redito obtuvo
> y el último es quien mas perdidas obtuvo.

</details>

### Método 2 ( Método de saldo positivo)

Buscamos luego un metodo que no dependa tanto del precio actual, para que no incida tan drasticamente en el score final.

Entonces pensamos en trabajar cada transaccion con un saldo de ethers nunca negativo. A este metodo lo denominamos "Metodo de saldo positivo".

Funciona de la sig. manera:

* Con cada taransaccion mantiene un saldo_positivo. Es decir, un saldo que nunca puede ser menor a 0.
* las primeras transacciones que fueran de venta, se descartan (porque darian saldo negativo de ethers)
* Las compras se efectuan por el total de ethers.
* Las ventas por el total siempre y cuando no deje saldo negativo, de lo contrario, permite solo vender lo que tiene.
* Si el saldo en ethers fuera positivo, se genernaria una transaccion ultima, de venta del total del saldo de ethers, asl ultimo valor de transaccion. 
  (Esta evita usar el valor actual del ether.)
 
Revisar Excel con casos, pestaña "Analisis 2"

Ventajas

* No depende del precio actual
* Medianamente simple de interpretar

Desventajas

* Puede dejar afuera cuentas buenas, porque su compra fue apenas anterior al comienzo de la ventana estudio.
* Puede deja afuera cuentas buenas, cuya venta es apenas posterior a la ventana de estudio por valores mejores al ultimo valor.
 


### Método 3 (Método de diferencia en ventas)

Este otro metodo, se basa en tomar solo aquellas cuentas que tuvieron reditos de las ventas.

Lo denominamos "Metodo de diferencia en ventas", se basa unicamente en el resultado directo obtenido de las ventas.
Es decir, con cada venta, calcula la diferencia en U$S con el valor promedio de compra, y mantiene registro de estas difreencias.
Las diferencias pueden ser positivas, si vende por encima del precio promedio de compra, o negativas en caso contrario.

Funciona de la siguiente manera:

* Con cada taransaccion mantiene un saldo_positivo. Es decir, un saldo que nunca puede ser menor a 0.
* las primeras transacciones que fueran de venta, se descartan (porque darian saldo negativo de ethers)
* Las compras se efectuan por el total de ethers. 
* Con cada compra, lleva cuenta del valor promedio de compra. 
* Las ventas por el total siempre y cuando no deje saldo negativo, de lo contrario, permite solo vender lo que tiene.
* Al momento de la venta, calcula la diferencia que obtuvo respecto al valor promedio. Esta diferencia puede ser positiva o negativa.
* Si queda saldo en ethers o no, no se tiene en cuenta ( a diferencia del metodo 1 y 2)
* Finalimente, el score se calcula como la sumatoria de las diferencias que obtuvo en todas las ventas.
* Estas diferencias se pueden mirar de manera absoluta (en U$S), o relativa al capital (U$S de la diferencia / U$S del capital)

Ventajas

* No depende del precio actual
* Medianamente simple de interpretar
* Se basea unicamente en cuentas que operan para sacar redito a plazo cierto (medio o largo de acuerdo a la ventana de estudio)

Desventajas

* Puede dejar afuera cuentas buenas, porque su compra fue apenas anterior al comienzo de la ventana estudio.
* Puede deja afuera cuentas buenas, cuya venta es apenas posterior a la ventana de estudio.


<details>
  <summary>Ampliar informacion sobre el Metodo 3</summary>

> ##  Metodo 3 (Metodo de diferencia en ventas)
> 
**Fecha** | **Cuenta** | **Ethers**   | **Valor** | **Saldo positivo**| **valor operable** | **obs** |
----------|------------|--------------|-----------|-------------------|--------------------|-------|
Fecha 1 | Cuenta A     |  100         |   $32.01  |  100              | 100                |  Compro 100 unidades a $32 (valor promeido $32)  |
Fecha 2 | Cuenta A     | - 20         |   $35.00  |   80              | -20                |  Vendio 20 unidades a $35. ( 20*(35-32) = +60) Gano +60 |
Fecha 3 | Cuenta A     |   10         |   $30.00  |   90              |  10                |  Compro 10 unidades a $30  (valor promedio (80*32 + 10*30) / (80+10) = 31,77   |
Fecha 4 | Cuenta A     | -200         |   $20.00  |    0              | -90                |  Vendio 90 unidades a 40 (90 *(20-31,77) = -1059,3 ) Perdio -1059 |
**TOTAL**   | Cuenta A |              |           |                   |                    |  −999,3 (perdio 999,3) |
> 
> En resumen, esta cuenta perdio 999,3 en las diferenicas por ventas que realizo.

</details>

### Estrategia elegida

 Despues de analizar las diferentes pruebas realizadas, optamos por el *Método 3*, porque es estable, simple y concreto.
 Es estable porque las variaciones del precio actual, no lo afectan.
 Simple, porque no requiere mecanismos complejos y se puede interpretar directamente.
 Y es concreto, porque solo mira el resultado en cada operacion. 

## Herramientas utilizadas

 Decidimos afrontar la solución desde 2 perspectivas distintas y similares al mismo en cuanto al uso de tecnologías, por un lado realizar una solución con:

Por un lado utlizar MongoDB como almacen de datos junto a un stack React / Node.js

y por el otro lado realizar una solución con el pack de herramientas de elasticsearch

## Aplicaciones realizadas

- [App React & Node.js & Mongo](https://gitlab.com/dgraselli/ether-nauta/-/tree/master/ethernauta_app)
- [elasticsearch](https://gitlab.com/dgraselli/ether-nauta/-/tree/scraping/scoring_elastic)

## Complicaciones en uso de las herramientos en la solución:

- [con App React & Node.js & Mongo](https://gitlab.com/dgraselli/ether-nauta/-/tree/master/ethernauta_app/readme.md#complicaciones)
- [con elasticsearch](https://gitlab.com/dgraselli/ether-nauta/-/blob/scraping/scoring_elastic/readme.md#complicaciones)

### Links de referencia

Los siguientes links fueron utilizados para consultar y analizar distintos aspectos de este proyecto.

* [BiqQuery](https://console.cloud.google.com/bigquery?sq=21854208155:08e01b51319a4ddcbb9826a98f3c6413) 
* [Kaggle notebook](https://www.kaggle.com/dgraselli/ethernauta-queries)
* [awesome-bigquery-views](https://github.com/RokoMijic/awesome-bigquery-views)
* [Goggle Big Query relacionada a Ethereum](https://console.cloud.google.com/marketplace/details/ethereum/crypto-ethereum-blockchain?pli=1)
* [Problamatica al Calcular Saldos](https://medium.com/google-cloud/how-to-query-balances-for-all-ethereum-addresses-in-bigquery-fb594e4034a7)
* [Sistema de recomendaciones](https://medium.com/google-cloud/building-token-recommender-in-google-cloud-platform-1be5a54698eb)
* [Kaggle Ethereum DATA](https://www.kaggle.com/bigquery/ethereum-blockchain)
* [Coinfi](https://www.coinfi.com/) Sitio de seguimiento de Criptomonedas por el creador de ethereum-etl Evgeny Medvedev (Mas de 2000 criptos ?!?!?)
* Chequear esta [Aragoon](https://mainnet.aragon.org/)
* [Pub/Sub bigQueyr tables at Google](https://medium.com/google-cloud/live-ethereum-and-bitcoin-data-in-google-bigquery-and-pub-sub-765b71cd57b5) Excelente articulo de Evgeny Medvedev para suscribir a eventos si queremos tener un sistema en linea. 
* Idem previo [real-time-ethereum-notifications-for-everyone-for-free](https://medium.com/google-cloud/real-time-ethereum-notifications-for-everyone-for-free-a76e72e45026)
* [Gini Calc](http://shlegeris.com/gini)
* [Google Datastudio Dashboard](https://datastudio.google.com/u/0/reporting/197An_pAkoJ_fAKCRTY6GO608j2mwz8CO/page/yJtg)
* https://www.adictosaltrabajo.com/2013/11/20/primeros-pasos-mongodb/
* https://docs.mongodb.com/manual/core/map-reduce/
* https://www.elastic.co/guide/en/elasticsearch/reference/current/docs.html
* https://stackoverflow.com/questions/35206409/elasticsearch-2-1-result-window-is-too-large-index-max-result-window
* https://www.elastic.co/guide/en/logstash/current/getting-started-with-logstash.html
* https://discuss.elastic.co/t/ending-logstash-after-csv-parsing/185232
* https://www.elastic.co/guide/en/kibana/6.8/index.html

