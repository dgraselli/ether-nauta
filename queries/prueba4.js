//db.aux.drop();
//db.aux.save(db.ctacte.find().limit(1000).toArray())
//newLimit = 10000 * 1024 * 1024; //104857600
//db.adminCommand({setParameter: 1, internalQueryMaxAddToSetBytes: newLimit})

db.scoring_m3.drop();
db.scoring_m3.save(
db.ctacte.aggregate([
    {$group: {_id: "$address", count: {$sum: 1}, balance_e: {$sum: "$ethers"}, balance_uss: {$sum: "$cost"} },
  ],
  {allowDiskUse: true }
)