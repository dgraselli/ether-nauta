a = [];
db.getCollection('top_txs').find().forEach( (x)=> a.push (x.address));

db.ctacte.aggregate([
//{$match: {address: '0x11180911f1852d08467bcf5fe41ac38580adf7ab'}},
//{$match: {address: '0x3f5ce5fbfe3e9af3971dd833d26ba9b5c936f0be'}},
{$match: {address: {$nin: a}}},
{$group: {_id: "$address", count: {$sum: 1}, txs: {$push: {ts: "$ts", ethers: "$ethers", price: "$price"}}}},
{$out: "add_txs"}
],
{allowDiskUse: true}
);