// VERIFICADO
// con cada compra, mantiene el valor promedio de compra y el saldo.
// con cada venta, se limita a solo tener en cuenta el saldo actual y calcula la direfencia como:
// DIFF = cant_lim_vendida * (precio_venta - precio_prom_compra)
// El resultado es la suma de las diferencias obtenidas
// 29' 
db.getCollection('ctacte').mapReduce(
  function(){ emit(this.address, {hash: this.hash, ts: this.ts, ethers: this.ethers, price: this.price}); },
  function(k,vs){ return vs.length },
  {
     query: {address: '0x0000000000333f28d3a20605c702f66c3143cccf'},
     out: "scoring_aux"
  }
);
