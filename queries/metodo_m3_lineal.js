txs = [];
obj = {};

list = db.ctacte.find().limit(0).sort({address:1, ts:1});

db.ctacte_m3.drop();
db.ctacte_m3.save(
list.map(
   function(x){

    if(last_address != x.address){

        /* 
        //guarda la cuenta previa si no es null
        if(last_address != null){
            print("SAVE" + last_address);
            db.address.save(
              {
                  address: last_address,
                  diff_acc: diff_acc,
                  diff_acc_per: diff_acc / uss_in,
                  balance_e: balance_e,
                  buy_avg: buy_avg,
                  op_in: op_in,
                  op_out: op_out,
                  uss_in: uss_in,
                  uss_out: uss_out,
                  score: diff_acc,
                  score2: diff_acc / uss_in,
                  }            
            );
            
       }
       */
       
       //reinicia los contadores 
       last_ts = 0;    //ultimo timestamp: se utiliza para garantizar que la ejeucion es cronologica
       balance_e = 0;  //balance positivo de ethers.
       buy_avg = null; //mantiene el valor promedio de compra
       diff_acc = 0;   //diferencia acumulada
    }

    //por cada fila

    var ts_diff = (last_ts==0)? 0 : (x.ts - last_ts);
    last_ts = x.ts;

    //garantiza que el orden es cronologico.   
    if (ts_diff < 0) {print(ts_diff); throw "Fallo el orden ???";};
        
    //guarda la diff en segundos
    ts_diff = ts_diff / 60000


    var op_out = 0;
    var op_in = 0;
    var uss_out = 0;
    var uss_in = 0;
    var diff = 0;
    var op = (x.ethers > 0) ? 'COMPRA' : 'VENDE';

    if(x.ethers < 0) {
        //vende. Controlo que no venda mas de lo que tiene
        e = Math.max(-balance_e, x.ethers);
        //si e > 0 es porque alguna vez ya compro y por lo tanto tiene buy_avg
        diff = -1 * e * (x.price - buy_avg);
        diff_acc += diff;
        op_in = 1;
        uss_in = -e * x.price; 
    }else{
        //compra. Toma todo.
        e = x.ethers;
        //calcula el nuevo promedio de compra
        buy_avg = (balance_e * buy_avg + e * x.price) / (balance_e + e);

        op_out = 1;
        uss_out = -e * x.price; 
    }
                 
    balance_e += e;
    
    last_address = x.address;
    

    //retorna la transaccion con datos del calculo
    x.op = op;
    x.buy_avg = buy_avg;
    x.e = e;
    x.balance_e = balance_e;
    x.diff = diff;
    //x.op_in =  op_in;
    //x.op_out =  op_out;
    //x.diff_acc = diff_acc;
    return x;
  }           
));
  
//db.getCollection('ctacte_m3').find({}, {_id:-0,address:1, op:1, ethers:1, buy_avg:1, e:1, diff:1, diff_acc:1}).sort({address:1})  