//Soulcion sugerida por Javier
//45' (solo el agrupamiento)
db.ctacte.mapReduce(
  function(){ emit(this.address, {tipo: 'basico', hash: this.hash, ts: this.ts, ethers: this.ethers, price: this.price}); },
  function(k,vs){ 
     ret = {list: [], cant_reduces: 1}
     vs.forEach( function(x) { 
        if(x.tipo=='basico'){ 
          ret.list.push(x);
        }    
        else{ 
          ret.list.concat(x.list);
          ret.cant_reduces += 1;
        }
      });
      
      return ret;
  },
  {
     //limit: 100, 
     query: {
           //address: '0x0000000000333f28d3a20605c702f66c3143cccf'
          },
     out: "scoring_aux_full",
     sort: {address:1, ts:1},     
     finalize: function(k,x){
           ret = {address: k, count:1, cant_reduces: x.cant_reduces, list: [x]};
           if(x.tipo != 'basico') {
             ret.count = x.list.length;
             ret.list = x.list;
           }
           return ret;         
         }
  }
);

/*  
a = db.scoring_aux_full.find().map(function(x){
    ret = {address: x._id, count:1, cant_reduces: 0, list: [x.value]};
    if(x.tipo != 'basico') {
        ret.count = x.value.list.length;
        ret.list = x.value.list;
        ret.cant_reduces = x.value.cant_reduces;
    }
    return ret;
    });
    
db.scoring_aux_full2.drop();
db.scoring_aux_full2.save(a);    
*/  
  