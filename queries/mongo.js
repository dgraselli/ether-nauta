db.getCollection('ctacte').aggregate([
{$group: {_id: "$address", 
    cost: {"$sum": "$cost"}, 
    ethers: {"$sum": "$ethers"}, 
    cant_op: {"$sum": 1}, 
    cant_compras: {"$sum": {$cond: {if: {$gt: ["$ethers",0]} ,then:1, else:0}}}, 
    cant_ventas: {"$sum": {$cond: {if: {$lt: ["$ethers",0]} ,then:1, else:0}}}, 
    }},
{$addFields: {
    saldo: {$sum: ["$cost",{$multiply: ["$ethers", 122]}]},
    }},
{$addFields: {
    saldo_en_millones: {$divide: ["$saldo", 1000000]},
    }},
{$sort: {saldo: 1}},
{$limit: 100},    
],
{ allowDiskUse: true }
)