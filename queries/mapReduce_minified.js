// VERIFICADO
//11 m
db.getCollection('ctacte').mapReduce(
  function(){ emit(this.address, {hash: this.hash, ts: this.ts, ethers: this.ethers, price: this.price}); },
  function(k,vs){ 
         r = 0;
         balance_e = 0;
         balance_uss = 0;
         last_price = 0;
         last_ts = 0;
         ts_diff= -1;
      
         vs = vs.sort(function(a, b){return a.ts - b.ts});
      
         for (i in vs) {             
             v = vs[i]; //tx
             
             //controlo que vaya en orden
             ts_diff = (ts_diff==-1)? 0 : (v.ts - last_ts);
             last_tf = v.ts;
             
             if (ts_diff < 0) throw "Fallo el orden ???";

             
             //si compra, puede comprar todo lo que quiera
             //si vende, controlo que no pueda vender mas de lo que tiene
             
             if(v.ethers < 0) {
                 //vende. Controlo que no venda mas de lo que tiene
                 e = Math.max(-balance_e, v.ethers);                 
             }else{
                 //compra. Toma todo.
                 e = v.ethers;
             }
             
             balance_e += e;
             uss = e * v.price * -1;
             balance_uss += uss;
             last_price = v.price;
          }
             
          //si resta saldo, los vendo al ultimo precio
          if(balance_e > 0){
              balance_uss += balance_e * last_price;
          }
             

         return balance_uss;
      },
  {
    query: {
       "$and": [    
           {address: { $in : ['0x8432b7ca18308e1ec9e4ef72619fe0f7de13a69e','0xe2f8e4c98f4d8ff3dc01bd1c01aafa57cc65e7cc'] },},
           {price: {$gt: 0}}
       ]
    },
    out: "tmp_scoring"
  }
);
