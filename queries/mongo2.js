db.getCollection('ctacte').mapReduce(
  function(){ emit(this.address, {ts: this.ts, ethers: this.ethers}); },
  function(k,vs){ 
         r = 0;
         balance_e = 0;
         balance_uss = 0;
         last_price = 0;
         last_ts = 0;
      
         for (i in vs) {             
             v = vs[i]; //tx
             
             //controlo que vaya en orden
             if (last_ts > v.ts) throw "Fallo el orden ???";

             
             //si compra, puede comprar todo lo que quiera
             //si vende, controlo que no pueda vender mas de lo que tiene
             
             if(v.ethers < 0) {
                 //vende. Controlo que no venda mas de lo que tiene
                 e = Math.max(0, balance_e + v.ethers);                 
             }
             
             balance_e += v.ethers;
             balance_uss += v.ethers * v.price * -1;
             last_price = v.price;
             }
             
          //si resta saldo, los vendo al ultimo precio
          if(balance_e > 0){
              balance_uss += balance_e * last_price;
          }
             
         return { "balance:" + balance_uss };
      },
  {
    query: {address: {$in : ['0x8432b7ca18308e1ec9e4ef72619fe0f7de13a69e','0xe2f8e4c98f4d8ff3dc01bd1c01aafa57cc65e7cc']}},
    out: "out_test"
  }
);
