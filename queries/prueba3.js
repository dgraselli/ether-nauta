//db.aux.drop();
//db.aux.save(db.ctacte.find().limit(1000).toArray())
//newLimit = 10000 * 1024 * 1024; //104857600
//db.adminCommand({setParameter: 1, internalQueryMaxAddToSetBytes: newLimit})

db.scoring_m3.drop();
db.scoring_m3.save(
db.ctacte.aggregate([
    //{$match: {address: '0x0000000000333f28d3a20605c702f66c3143cccf'}},
    {$sort: {address:1, ts:1}},
    {$group: {_id: "$address", count: {$sum: 1}, txs: {$push: {ts: "$ts", ethers: "$ethers", price: "$price"}}}},
  ],
  {allowDiskUse: true }
).map(function(x){
        
         balance_e = 0;
         buy_avg = 0;
         diff = 0;
         acc_diff = 0;
         last_ts = 0;
         ts_diff = -1;
         txs = [];

         for (i in x.txs) {             
             v = x.txs[i]; //tx
             
             //controlo que vaya en orden
             ts_diff = (ts_diff==-1)? 0 : (v.ts - last_ts);
             last_ts = v.ts;
             
             if (ts_diff < 0) throw "Fallo el orden ???";

             
             //si compra, puede comprar todo lo que quiera
             //si vende, controlo que no pueda vender mas de lo que tiene
             
             if(v.ethers < 0) {
                 //vende. Controlo que no venda mas de lo que tiene
                 e = Math.max(-balance_e, v.ethers);
                 //si e > 0 es porque alguna vez ya compro y por lo tanto tiene buy_avg
                 diff = -1 * e * (v.price - buy_avg)
                 acc_diff += diff;
             }else{
                 //compra. Toma todo.
                 e = v.ethers;
                 //calcula el nuevo promedio de compra
                 buy_avg = (balance_e * buy_avg + e * v.price) / (balance_e + e);
             }
             
             balance_e += e;
             
             txs.push( 
                {
                 //hash: v.hash,   
                 ethers: v.ethers,
                 e: e,
                 uss: e * v.price,
                 price: v.price,
                 balance_e : balance_e,
                 buy_avg: buy_avg,
                 diff: diff,
                 ts: v.ts,    
                 date: new Date(v.ts * 1000),    
                 last_tx_in_hours: ts_diff / 3600,
                 last_tx_in_sec: ts_diff,
                 });
                 
          } //endfor
          
         sum_e_in   = txs.map((t) => t.e).filter((e) => e>0).reduce((p,c) => p+c,0);
         sum_e_out  = txs.map((t) => t.e).filter((e) => e<0).reduce((p,c) => p+c,0);
         count_in   = txs.map((t) => t.e).filter((e) => e>0).reduce((p,c) => p+1,0);
         count_out  = txs.map((t) => t.e).filter((e) => e<0).reduce((p,c) => p+1,0);
         sum_uss_in = txs.map((t) => t.uss).filter((e) => e>0).reduce((p,c) => p+c,0);
         sum_uss_out= txs.map((t) => t.uss).filter((e) => e<0).reduce((p,c) => p+c,0);

   
          return { 
             address: x._id,
             score: sum_uss_in > 0? acc_diff / sum_uss_in * 100 : null,
             acc_diff: sum_uss_in > 0? acc_diff : NaN,
             txs_count: x.txs.length,
             balance_e  : balance_e,
             acc_diff: acc_diff,
             buy_avg: buy_avg,
             sum_e_in   : sum_e_in,
             sum_e_out  : sum_e_out,
             count_in   : count_in,
             count_out  : count_out,
             sum_uss_in : sum_uss_in,
             sum_uss_out: sum_uss_out,
             txs: txs,
             //score es el balance en uss sobre el total de uss invertidos
             //"score": balance_uss / txs.map((t) => t.uss).filter((e) => e>0).reduce((p,c) => p+c,0)
           };
           
       } //end map function
).filter(function(x){return x.score != NaN}))