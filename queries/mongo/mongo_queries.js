db.v_addresses_top.drop();
db.v_addresses_bottom.drop();


//CUENTAS MAS EXISTOSAS
// sobre las 1000 cuentas que mas ganaaron
// me quedo con las 10 que mayor porcentaje de inversion obtuvieron
db.createView("v_top_addresses", "addresses",
 [
  {$project: {address:1, diff:1, diff_per:1}},
  {$sort: {diff: -1}},
  {$limit: 100},
  {$sort: {diff_per: -1}},
  {$limit: 10},
 ])

//CUENTAS Menos EXISTOSAS
// sobre las 1000 cuentas que mas ganaaron
// me quedo con las 10 que mayor porcentaje de inversion obtuvieron
db.createView("v_addresses_bottom", "addresses",
 [
  {$project: {address:1, diff:1, diff_per:1}},
  {$sort: {diff: 1}},
  {$limit: 100},
  {$sort: {diff_per: 1}},
  {$limit: 10},
])

