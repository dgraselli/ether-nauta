# Aplicación de scoreo realizada con el pack de elasticsearch free

## Explicación del proceso (Logstash + Elasticsearch)

### * Carga de precios

 Una vez obtenidos los datos iniciales explicados en el readme principal del proyecto, con logstash se realizó el procesamiento del csv de precios para almacenarlos en un indice de elasticsearch, dicho archivo de configuración se encuentra [aqui](https://gitlab.com/dgraselli/ether-nauta/-/blob/scraping/scoring_elastic/logstash_confs/step_0_prices.conf).
Como filtro se aplicó calcular la medias de los precios de compra y venta y ademas crear un nuevo field `datetime_operation` para zonificarlo para ser precisos en la busqueda mas adelante.

### * Carga de transacciones
 Este proceso es simil al anterior y cumple el mismo objetivo: generar un indice de transacciones en elasticsearh, el archivo de configuración de logstash se encuentra [aqui](https://gitlab.com/dgraselli/ether-nauta/-/blob/scraping/scoring_elastic/logstash_confs/step_1_all_txs.conf).
 Como filtro lo que se va a realizar es:
 * Eliminar las columnas de los datos que no nos interesan
 * Quedarnos con las transacciones de status 1, es decir exitosas en la blockchain
 * Quedarnos con las trancciones de wallets, es decir que no sean smart contracts
 * Hacer conversiones de datos
 * Zonificar el `block_timestamp` de la transacción, para que luego la búsqueda en el indice de precios creado en el proceso anterior sea precisa
 * Convertir `amount_eth`
 * Crear un nuevo field llamado `new_date`. Este field es creado por este [filtro de ruby](https://gitlab.com/dgraselli/ether-nauta/-/blob/scraping/scoring_elastic/rb_code/format_date.rb) que lo que hace es formar en base al `block_timestamp` el datetime previo con minuto multiplo de 5, para poder tomar el precio estimativo previo a la transacción. Recordemos que los precios están en una ventana de 5M.
 
Al finalizar este proceso tendremos creado el indice de transacciones dentro de elasticsearch que ademas de poseer información de la transaccion la enriquecimos con el precio de compra, precio de venta, etc desde el indice de precios.


### * Carga de movimientos(compras/ventas)

En este proceso se utilizara el indice de transacciones creado anteriormente y si creara un movimiento en el indice de movimientos, la finalidad de este proceso es distinguir las compras de las ventas para luego poder scorear todo.
Está subdividido en 2 procesos logstash es decir la [carga de compras](https://gitlab.com/dgraselli/ether-nauta/-/blob/scraping/scoring_elastic/rb_code/format_date.rb) y la [carga de ventas](https://gitlab.com/dgraselli/ether-nauta/-/blob/scraping/scoring_elastic/logstash_confs/step_2_sells.conf). Ambos procesos hacen practicamente lo mismo con la diferencia que para `from_address` es una venta y para `to_address` una compra, eso se diferencia con `S`(sell) y `B`(buy) repectivamente.
Este proceso creara movimientos sonbre el indice de movimientos tomando el precio correspondiente en caso de venta o compra del indice del proceso anterior y marcandola como tal.

### * Carga de wallets(addresses)

Este proceso, donde su configuracion de losgtash se encuentra [aqui](https://gitlab.com/dgraselli/ether-nauta/-/blob/scraping/scoring_elastic/logstash_confs/step_3_address.conf), lo que realiza en un indice de addresses donde el id del documento es propiamente la address, para evitar tener registros repetidos, este indice se utiliza en el proceso de Scoreo. Se hará un comentario respecto a este diseño en el apartado [Complicaciones](https://gitlab.com/dgraselli/ether-nauta/-/edit/scraping/scoring_elastic/readme.md#complicaciones) 

### * Scoreo

El proceso de scoreo genera el indice de scoring y es el mas costoso en cuanto a tiempo de ejecución, se necesita que por los menos 1 vez se hayan ejecutado los procesos predecesores.
El archivo de configuración de logstash se encuentra [aqui](https://gitlab.com/dgraselli/ether-nauta/-/blob/scraping/scoring_elastic/logstash_confs/step_4_scoring.conf).

Lo que se realiza es la obtención de todas las wallets desde el indice creado y cargado en el proceso anterior, y la captura de todos los movimientos de cada wallet desde el indice de moviemientos ordenadas por el tiempo de operación. Dicha query se encuentra [acá](https://gitlab.com/dgraselli/ether-nauta/-/blob/scraping/scoring_elastic/templates/template.json). Una vez obtenidos todos los movimientos de una wallet se procesa el evento para poder scorear a dicha wallet, según la [estrategia de scoreo](https://gitlab.com/dgraselli/ether-nauta/-/tree/re_edit_readme#estrategia-elegida) indicada previamente. Dicha estrategia está implemetada en el siguiente [filter de ruby](https://gitlab.com/dgraselli/ether-nauta/-/blob/scraping/scoring_elastic/rb_code/create_moves.rb) 


### Explicación gráfica del proceso
<img src="https://gitlab.com/dgraselli/ether-nauta/-/raw/scraping/scoring_elastic/imgs/logstash.png" heigth="500" width="800"/>



## Kibana
Kibana mostrará datos leyendo información, del index scoring, el dashboard desarrollado se muestra en la siguiente imagen y temporalmente esta instalado una demo en gcloud [aqui](https://75ceb924f6374e6da72679f7346a36e6.us-central1.gcp.cloud.es.io/app/kibana):

user:

pass:

<img src="https://gitlab.com/dgraselli/ether-nauta/-/raw/scraping/scoring_elastic/imgs/dashboard.png" heigth="600" width="900" />


### Explicación gráfica del proceso
<img src="https://gitlab.com/dgraselli/ether-nauta/-/raw/scraping/scoring_elastic/imgs/kibana.png" heigth="500" width="800"/>


## Manos a la obra



### Ejecucion de logstash para cargar los indices

Nota1: se deberá tener instalado en la máquina donde se ejecuten los scripts: `logstash, elasticsearh y kibana`

Nota2: las versiones utilizadas para este desarrollo son: `kibana 6.8.7 elasticsearch 6.8.7 logstash 7.6.1`

Nota3: es importante que el nombre del indice generado por el proceso scoring, es decir el valor de la variable de entorno `INDEX_SCORING`, sea `scoring`.

1. Seteo de variables de entorno: 
    Para poder empezar a correr los procesos con logstash se necesitaran definir las siguientes variables de entorno:
            
    * `TRANSACTIONS_CSV_PATH` -> que va a ser el path del [archivo de transacciones](https://drive.google.com/file/d/1hqADdU8AlR7ayABYLsi0TnyAsibnyLQa/view?usp=sharing) donde se hizo la descarga.
                
               
    *  `PRICES_CSV_PATH` -> que a ser el path del [archivo de precios](https://drive.google.com/file/d/1WQNKqpC-47w_e8C0yJ9VxdTEGHwb4fJE/view?usp=sharing) donde se hizo la descarga
                
               
    *  `PROJECT_PATH` -> el path completo hasta el directorio scoring_elastic, es decir hasta la applicación
                
                
    * los indices: `INDEX_PRICES`, `INDEX_TRANSACTIONS`, `INDEX_WALLET_MOVEMENTS`, `INDEX_ADDRESSES`, `INDEX_SCORING`
                
               
    *  y el host de elastic -> `ELASTIC_HOST`

    un ejemplo hay [aqui](https://gitlab.com/dgraselli/ether-nauta/-/blob/scraping/scoring_elastic/env_vars.sh) y simplemente con editarlo localmente el archivo y correrlo en bash bastaría:
    `bash env_vars.sh`

2. Carga de precios

    Para la carga de precios bastará con ejecutar `bash step1.sh` donde step1.sh está [aqui](https://gitlab.com/dgraselli/ether-nauta/-/blob/scraping/scoring_elastic/step1.sh)
    
    Este proceso no termina solo al hacer una lectura con logstash desde un archivo, hay que matarlo manualmente una vez que no muestre mas info en la salida. Esto tal vez es por la falta de experiencia en la herramienta

3. Carga de transacciones

    Idem anterior, haciendo `bash step2.sh` que se encuentra [aqui](https://gitlab.com/dgraselli/ether-nauta/-/blob/scraping/scoring_elastic/step2.sh)
    
    Como es una carga desde un archivo, también hay que matar el proceso una vez que no muestra nada en la salida.

4. Carga del resto:

    El resto de los procesos logstash se pueden ejecutar todos desde un script armado llamado [step3.sh](https://gitlab.com/dgraselli/ether-nauta/-/blob/scraping/scoring_elastic/step3.sh) haciendo simplemente `bash step3.sh`

### Importacion en kibana

1. Carga de visualizaciones

    Las visualizaciones realizadas son:

* [el conteo de cuentas](https://gitlab.com/dgraselli/ether-nauta/-/blob/scraping/scoring_elastic/kibana/visualization_accounts.json) que cumplen con el filtro aplicado.
* [un gráfico de barras horizontal](https://gitlab.com/dgraselli/ether-nauta/-/blob/scraping/scoring_elastic/kibana/visualization_bar.json) que agrupa cuentas segun el score.
* [los controles](https://gitlab.com/dgraselli/ether-nauta/-/blob/scraping/scoring_elastic/kibana/visualization_control.json) que permiten filtrar por alguna address particular, por un rango de score y por un rango de ganancia
* [una tabla](https://gitlab.com/dgraselli/ether-nauta/-/blob/scraping/scoring_elastic/kibana/visualization_table.json) donde se ve la información de las cuentas

Para realizar las instalaciones de las visualizaciones basta con ir a `kibana-> management -> saved objects -> import` y allí subir los .json anteriormente mencionados.

2. Carga del dashboard

El dashboard realizado se encuentra [aqui](https://gitlab.com/dgraselli/ether-nauta/-/blob/scraping/scoring_elastic/kibana/dashboard_kibana.json) viene con algunos filtros predefinidos:
* ganancia mayor a 500usd
* porcentaje promedio de ganancia mayor a 4%
* score mayor a 1

Para realizar la instalación del dashboard es el mismo proceso que las visualizaciones



# Complicaciones

## Logstash

* No pude hacer que finalicen los procesos de lectura de los archivos .csv, es decir sacar a logstash en modo `tail` cuando se trata de archivos.
* los input de elastic no permiten hacer agregaciones, esto me limito bastante para el scoreo, por lo que tuve que hacer un indice especial (addresses) donde no tenga address repetidas para traer los momivientos por address, para ello puse como _id del documento a la address.
* Los filter de elastic no permiten hacer queries complejas, solo sencillas. Por ej no permite hacer un paginado usando from y size


## Elasticsearh 

* Como habia cuentas con muchisimos movimientos(>100k/mes) ([ver analisis](https://gitlab.com/dgraselli/ether-nauta/-/blob/scraping/analisis.md)) esto me limitó bastante ya que en mi máquina, al cambiar el valor en el indice de moviemientos de la variable max_result_window en el indice por uno muy grande, fallaba el server local de elastic. Realicé el despliegue de todo en Gcloud para porder descartar esa limitacion de memoria,  pero Gcloud limita el tamaño de la respuesta de la api en 1.8GB. Por lo que limite el size finalmente a 10k movimientos, esta limitación no es del todo mala ya que en 30 dias tener 10k de movimientos es una buena cantidad de movimientos. 
