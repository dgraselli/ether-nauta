config_buys=${PROJECT_PATH}"logstash_confs/step_2_buys.conf"
config_sells=${PROJECT_PATH}"logstash_confs/step_2_sells.conf"
config_address=${PROJECT_PATH}"logstash_confs/step_3_address.conf"
config_scoring=${PROJECT_PATH}"logstash_confs/step_4_scoring.conf"
logstash -f $config_buys
logstash -f $config_sells
logstash -f $config_address
logstash -f $config_scoring