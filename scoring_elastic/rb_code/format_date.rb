def filter(event)
    d = Time.at(event.get("datetime_operation").to_i)
    minute = d.min
    new_minute = (minute.div(5)) * 5
    new_date = DateTime.new(d.year,d.month,d.day,d.hour,new_minute)
    event.set("new_date", new_date.to_time.to_i)
    return [event]
end

