
def get_position_first_buy(event)
    pos = 0
    for op in event.get("type_ops")
        if op == "B"
            return pos
        end
        pos += 1 
    end
    return -1
end

def generate_first_move(event, pos)
    row = {
        "op_type" => "B",
        "amount_op_eth" => event.get("amount_eths")[pos].to_f.round(3),
        "balance_after_eth" => event.get("amount_eths")[pos].to_f.round(3),
        "sum_price_b" =>event.get("price_eths")[pos].to_f.round(3),
        "price_sell" => 0.0.to_f,
        "ops_complete" => 0,
        "avg_amount_b" => event.get("price_eths")[pos].to_f.round(3),
        "score" => 0,
        "profit" =>0.0.to_f,
        "sum_profit" => 0.0,
        "sum_percent_profit" =>0.0.to_f,
        "avg_percent_profit"=>0.0.to_f,
        "success_ops" => 0,
        "amount_op_b" => 1,
        "buys_ops" =>1,
        "sells_ops" => 0,
        "sum_score" =>0,
        "datetime" => event.get("datetime_operations")[pos]
    }
    return [row]
end

def update_balance_after(event, scoring_table, pos)

    return event.get("amount_eths")[pos].to_f + scoring_table.last["balance_after_eth"].to_f
end

def update_sum_price_b(event, scoring_table, pos)
    price_eths = event.get("price_eths")[pos].to_f
    return price_eths + scoring_table.last["sum_price_b"].to_f

end

def update_avg_amount_b(event, scoring_table, pos)
    
    return update_sum_price_b(event, scoring_table, pos) / (scoring_table.last["amount_op_b"] +1)
end
def acumulation_and_update_table(event, scoring_table, pos)
        row = {
            "op_type" => "B",
            "amount_op_eth" => event.get("amount_eths")[pos].to_f.round(3),
            "balance_after_eth" => update_balance_after(event, scoring_table, pos).round(3),
            "sum_price_b" =>update_sum_price_b(event, scoring_table, pos).round(3),
            "price_sell" => 0,
            "ops_complete" => scoring_table.last["ops_complete"],
            "avg_amount_b" => update_avg_amount_b(event, scoring_table, pos).round(3),
            "score" => 0,
            "profit" => scoring_table.last["profit"],
            "sum_profit" => scoring_table.last["sum_profit"],
            "sum_percent_profit" =>scoring_table.last["sum_percent_profit"].round(3),
            "avg_percent_profit" =>scoring_table.last["avg_percent_profit"],
            "success_ops" =>scoring_table.last["success_ops"],
            "amount_op_b" => scoring_table.last["amount_op_b"] +1,
            "buys_ops" => scoring_table.last["buys_ops"] +1,
            "sells_ops" => scoring_table.last["sells_ops"],
            "sum_score" => scoring_table.last["sum_score"],
            "datetime" => event.get("datetime_operations")[pos]
        }
        scoring_table.clear
        scoring_table.push(row)
end
def get_score_sell(event, scoring_table, pos)
    score_op = 0
    
    if scoring_table.last["avg_amount_b"].to_f < event.get("price_eths")[pos].to_f
        score_op = 1
    elsif scoring_table.last["avg_amount_b"].to_f > event.get("price_eths")[pos].to_f
        score_op = -1
    end
    return score_op
end
def get_price(event, scoring_table, pos)
    price = event.get("price_eths")[pos]
    return price.to_f
end
def get_profit_sell(event, scoring_table, pos)
    if (get_score_sell(event, scoring_table, pos) == 1)
        sell_value = (get_price(event, scoring_table, pos) * get_amount_op_sell(event, scoring_table, pos).to_f)
        buy_value = get_amount_op_sell(event, scoring_table, pos).to_f * scoring_table.last["avg_amount_b"].to_f
        profit = sell_value - buy_value
        return profit.to_f
    end
    return 0
end


def get_sum_profit(event, scoring_table, pos)
    
    return scoring_table.last["sum_profit"].to_f + get_profit_sell(event, scoring_table, pos)
end

def sum_percent_profit(event, scoring_table, pos)
    new_percent = 0
    if get_score_sell(event, scoring_table, pos) == 1
        sell_value = (event.get("price_eths")[pos].to_f * get_amount_op_sell(event, scoring_table, pos).to_f)
        buy_value = (get_amount_op_sell(event, scoring_table, pos).to_f * scoring_table.last["avg_amount_b"].to_f)
        if buy_value >0 and sell_value >0
            new_percent = (sell_value * 100 / buy_value) - 100
        end
    end
    return scoring_table.last["sum_percent_profit"].to_f + new_percent.to_f
end

def get_success_ops(event, scoring_table, pos)
    success_ops = scoring_table.last["success_ops"]
    if get_score_sell(event, scoring_table, pos) == 1
        success_ops += 1
    end
    return success_ops
end

def get_avg_percent_profit(event, scoring_table, pos)
    success_ops = get_success_ops(event, scoring_table, pos)
    sum = sum_percent_profit(event, scoring_table, pos)
    if success_ops > 0 and sum > 0
        return  sum / success_ops
    end
    return 0
end

def get_amount_op_sell(event, scoring_table, pos)
    amount = event.get("amount_eths")[pos].to_f
    balance = scoring_table.last["balance_after_eth"].to_f
    if amount > balance
        return balance
    end
    return amount
end
def get_row_sell_all(event, scoring_table, pos)
        row = {
            "op_type" => "S",
            "amount_op_eth" => get_amount_op_sell(event, scoring_table, pos).round(3),
            "balance_after_eth" => 0.0,
            "sum_price_b" =>0.0,
            "price_sell" => event.get("price_eths")[pos].to_f.round(3),
            "ops_complete" => scoring_table.last["ops_complete"] +1,
            "avg_amount_b" => 0.0,
            "score" => get_score_sell(event, scoring_table, pos),
            "sum_score" => scoring_table.last["sum_score"] + get_score_sell(event, scoring_table, pos),
            "profit" => get_profit_sell(event, scoring_table, pos).round(3),
            "sum_profit" => get_sum_profit(event, scoring_table, pos).round(3),
            "sum_percent_profit" =>sum_percent_profit(event, scoring_table, pos).round(3),
            "success_ops" => get_success_ops(event, scoring_table, pos),
            "avg_percent_profit"=>get_avg_percent_profit(event, scoring_table, pos).round(3),
            "amount_op_b" => 0,
            "buys_ops" => scoring_table.last["buys_ops"],
            "sells_ops" => scoring_table.last["sells_ops"] +1,
            "datetime" => event.get("datetime_operations")[pos]
        }
        return row
end

def get_row_sell_part(event, scoring_table, pos)
    row = get_row_sell_all(event, scoring_table, pos)
    row["balance_after_eth"] = scoring_table.last["balance_after_eth"] - event.get("amount_eths")[pos].to_f
    row["sum_price_b"] = scoring_table.last["sum_price_b"]
    row["avg_amount_b"] = scoring_table.last["avg_amount_b"]
    return row
end

def sell_and_update_table(event, scoring_table, pos)
    row = {}
    if event.get("amount_eths")[pos].to_f >= scoring_table.last["balance_after_eth"]
        # sell all
        row = get_row_sell_all(event, scoring_table, pos)
    else
        # sell a part
        row = get_row_sell_part(event, scoring_table, pos)
    end
    scoring_table.clear
    scoring_table.push(row)
end

def process_rest_moves(event, scoring_table, start_position)
    i = start_position
    while i < event.get("type_ops").length do
        if !event.get("price_eths")[i].nil?
            if event.get("type_ops")[i] == "B"
                acumulation_and_update_table(event, scoring_table, i)
            else
                sell_and_update_table(event, scoring_table, i)
            end
        end
        i+=1
    end
end

def generate_data_for_event(event, scoring_table)
    if scoring_table.length > 0
        last_element = scoring_table.last
        event.set("sum_score", last_element["sum_score"])
        event.set("avg_percent_profit", last_element["avg_percent_profit"].round(3))
        event.set("sum_profit", last_element["sum_profit"].round(3))
        event.set("buys_ops", last_element["buys_ops"])
        event.set("sells_ops", last_element["sells_ops"])
        #event.set("moves", scoring_table)
    end
end
def create_score(event)
    position_first_buy = get_position_first_buy(event)
    if position_first_buy != -1
        scoring_table = generate_first_move(event, position_first_buy)
        process_rest_moves(event, scoring_table, position_first_buy +1)
        generate_data_for_event(event, scoring_table)
    end
end

def filter(event)
    tob = event.get("type_ops")
    if (tob.kind_of?(Array)) and tob.length > 1
        create_score(event)
    end
    return [event]
end